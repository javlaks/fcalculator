/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc.algorithms;

import com.google.common.io.CharStreams;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import org.pushingpixels.lafwidget.utils.LafConstants;
import vic.tor.pf.ex.ErrorData;
import vic.tor.pf.ui.VStatusBar;

/**
 *
 * @author Victor
 */
public enum PasswordStrength {
    POOR,
    WEAK,
    MEDIUM,
    GOOD,
    EXCELLENTE;

    private InputStream passwordFile;
    private List<String> readLines = null;
    private String[] partialRegexChecks = {
        ".*[a-z]+.*", // lower
        ".*[A-Z]+.*", // upper
        ".*[\\d]+.*", // digits
        ".*[@#$%]+.*" // symbols
    };

    public boolean isInDictionary(String password) {
        String word = password;
        if(passwordFile == null) {
            try {
                passwordFile = getClass().getResourceAsStream("/org/fc/files/passq.lst");
                readLines = CharStreams.readLines(new InputStreamReader(passwordFile));
            } catch(IOException ex) {
                new ErrorData(ex).showErrorInfo();
                return true;
            }
        }
        for(String string : readLines) {
            if(string.equals(word)) {
                VStatusBar.setWarningMessage("La contraseña: " + word + " no es segura");
                return true;
            }
        }
        return false;
    }

    private int checkPasswordStrength(String password) {
        int strengthPercentage = 0;

        if(password.matches(partialRegexChecks[0])) {
            strengthPercentage += 25;
        }
        if(password.matches(partialRegexChecks[1])) {
            strengthPercentage += 25;
        }
        if(password.matches(partialRegexChecks[2])) {
            strengthPercentage += 25;
        }
        if(password.matches(partialRegexChecks[3])) {
            strengthPercentage += 25;
        }

        return strengthPercentage;
    }
    
    public String checkPassword(String password){
        if(isInDictionary(password)){
            return "muy débil";
        }
        int value = checkPasswordStrength(password);
        
        if(value < 25){
            return "muy débil";
        }
        if(value < 50){
            return "débil";
        }
        if(value < 75){
            return "aceptable";
        }
        if(value < 100){
            return "fuerte";
        }
        
        return "muy fuerte";
    }

}
