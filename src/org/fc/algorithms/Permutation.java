/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc.algorithms;

import com.google.common.base.Strings;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Galdino
 */
public class Permutation {
    private static List<String> permutationsList;

    private static List<Character> getList(String word) {
        List<Character> list = new ArrayList<>(word.length());
        for(int i = 0; i < word.length(); i++) {
            list.add(word.charAt(i));
        }
        return list;
    }

    private static void generate(String word, List<Character> list) {
        if(list.size() == 1) {
            permutationsList.add(word + list.get(0));
        }
        for(int i = 0; i < list.size(); i++) {
            Character b = list.remove(i);
            generate(word + b, list);
            list.add(i, b);
        }
    }

    public static List<String> getPermutations(String word) {
        permutationsList = new ArrayList<>();
        generate("", getList(word));
        return permutationsList;
    }

    public static List<String> getPhonePermutations(String areaCode) {
        permutationsList = new ArrayList<>();
        for(int i = 0; i <= 9999999; i++) {
            String number = areaCode + Strings.padStart(String.valueOf(i), 7, '0');
            permutationsList.add(number);
        }
        return permutationsList;
    }

    private Permutation() {
    }
}
