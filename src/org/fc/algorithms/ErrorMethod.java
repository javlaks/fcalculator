/*
 * @(#)ErrorMethod.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.algorithms;

import org.fc.utils.data.Constants;

/**
 * This class contains the methods for evaluate the error relative and absolute
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 1.1
 * @version 1.2
 */
public class ErrorMethod {
    private String res = "";
    private double errorAbsoluto, errorRelativo;
    private String salto2 = Constants.salto2;

    public void CalcularError(double valV, double valA) {

        errorAbsoluto = (valV - valA);
        errorRelativo = ((errorAbsoluto / valV) * 100);

        setRes("Error Absoluto: " + errorAbsoluto + salto2 + "Error Relativo: " + errorRelativo + " %");
    }

    /**
     * @return the res
     */
    public String getRes() {
        return res;
    }

    /**
     * @param res the res to set
     */
    public void setRes(String res) {
        this.res = res;
    }
}
