/*
 * @(#)Statistics.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class contains elements for manager data of Statistics
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public class Statistics extends ArrayList<Double> {
    private static final long serialVersionUID = -3945490907897826412L;
    private double temp;
    private boolean isSort;

    public Statistics() {
    }

    @Override
    public void add(int index, Double element) {
        setIsSort(false);

        super.add(index, element);
    }

    public void addElement(Double element) {
        setIsSort(false);
        super.add(element);
    }

    public void addElement(Integer element) {
        setIsSort(false);
        super.add(element.doubleValue());
    }

    public void addElement(String element) {
        setIsSort(false);
        super.add(Double.valueOf(element));
    }

    public void sort() {
        Collections.sort(this.subList(0, size()));
        setIsSort(true);
    }

    public double sum() {
        temp = 0;
        for(int i = 0; i < size(); i++) {
            temp += get(i);
        }

        return temp;
    }

    public double arithmeticMean() {
        temp = 0;
        temp = sum() / size();

        return temp;
    }

    public double cuadraticMean() {
        temp = 0;
        for(int i = 0; i < size(); i++) {
            temp += Math.pow(get(i), 2);
        }
        temp = Math.sqrt((temp / size()));

        return temp;
    }

    public double harmonicaMean() {
        temp = 0;

        for(int i = 0; i < size(); i++) {
            temp += get(i);
        }

        temp /= size();

        return temp;
    }

    /**
     * Este metodo devuelve el limite inferior del vector.
     *
     * @return Limite Inferior
     */
    public double getLowerLimit() {
        if(isIsSort()) {
            return get(0);
        } else {
            return 0;
        }
    }

    /**
     * Este metodo devuelve el limite superior del vector.
     *
     * @return Limite Superior
     */
    public double getUpperLimit() {
        if(isIsSort()) {
            return get(this.size() - 1);
        } else {
            return 0;
        }
    }

    /**
     * Este metodo nos devuelve el rango entre el limite superior e inferior.
     *
     * @return Diferiencia entre LS y LI
     */
    public double getRank() {
        if(isIsSort()) {
            return getUpperLimit() - getLowerLimit();
        } else {
            return 0;
        }
    }

    public double middle() {
        temp = 0;
        if(isIsSort()) {
            if(size() % 2 == 0) {
                int med1 = size() / 2;
                int med2 = (size() / 2) - 1;
                double temp1 = get(med1);
                double temp2 = get(med2);
                temp = (temp1 + temp2) / 2;
            } else {
                int med = ((size() + 1) / 2) - 1;
                temp = get(med);
            }
        }

        return temp;
    }

    public double standardDeviation() {
        temp = 0;
        for(int i = 0; i < size(); i++) {
            temp += Math.pow(get(i) - arithmeticMean(), 2);
        }
        temp /= size();
        temp = Math.sqrt(temp);

        return temp;
    }

    public double variance() {
        temp = 0;
        temp = Math.pow(standardDeviation(), 2);

        return temp;
    }

    public String moda() {
        int repeticiones = 0;
        List<Double> modaa = new ArrayList<>();
        Map<Double, Double> m = new HashMap<>();
        Iterator<Double> en = this.iterator();
        while(en.hasNext()) {
            Double val = en.next();
            if(m.containsKey(val)) {
                m.put(val, m.get(val) + 1);
            } else {
                m.put(val, 1.0);
            }
        }

        Iterator<Entry<Double, Double>> iter = m.entrySet().iterator();
        while(iter.hasNext()) {
            Entry<Double, Double> e = iter.next();
            if(e.getValue() > repeticiones) {
                modaa.clear();
                modaa.add(e.getKey());
                repeticiones = e.getValue().intValue();
            } else if(e.getValue() == repeticiones) {
                modaa.add(e.getKey());
            }
        }

        if(modaa.size() == this.size()) {
            return "No hay moda";
        } else {
            return String.valueOf(modaa);
        }
    }

    /**
     * @return isSort Valor para saber si esta ordenado
     */
    public boolean isIsSort() {
        return isSort;
    }

    /**
     * @param isSort Poner true si esta ordenado o false en caso contrario
     */
    public void setIsSort(boolean isSort) {
        this.isSort = isSort;
    }
}
