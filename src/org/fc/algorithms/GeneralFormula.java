/*
 * @(#)GeneralFormula.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.algorithms;

import org.fc.utils.data.Constants;

/**
 * This class contains the necesary method for evaluate an ecuation by general formula or cuadratic
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 1.0
 * @version 4.0
 */
public class GeneralFormula {
    //Declaracion de variables en la cuales nos vamos a apoyar
    private static String salto = Constants.salto;
    private static double d, e;

    /**
     * No apoyamos en este metodo para resolver la ecuacion. Recibe 3 parametros y tiene un valor de
     * retorno tipo String.
     *
     * @param a
     * @param b
     * @param c
     *
     * @return String
     */
    public static String metodofg(double a, double b, double c) {
        d = Math.sqrt(Math.pow(b, 2) - (4 * a * c));
        e = Math.sqrt(Math.pow(b, 2) - (4 * a * c));

        if(d >= 0) {
            d = ((-b + d) / (2 * a));
            e = ((-b - e) / (2 * a));

            return "X1= " + d + salto + "X2= " + e;
        } else {
            return "No se puede resolver esta ecuación cuadrática con los valores:" + salto + "a = " + a + salto + "b = " + b + salto + "c = " + c;
        }
    }

    private GeneralFormula() {
    }
}