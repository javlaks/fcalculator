/*
 * @(#)RandomNumber.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.algorithms;

/**
 * This class contains the methods to determinate the random numbers
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 2.0
 * @version 3.1
 */
public class RandomNumber {
    private static String res;
    private static long entero;
    private static double decimal;

    public static void entero(long inicio, long fin) {
        do {
            entero = (long) (Math.random() * fin);
        } while(inicio >= entero);

        setRes(String.valueOf(entero));
    }

    public static void decimal(double inicio, double fin) {
        do {
            decimal = (Math.random() * fin);
        } while(inicio >= decimal);

        setRes(String.valueOf(decimal));
    }

    public static int nextRandom(int begin, int end) {
        int val;
        do {
            val = (int) (Math.random() * end);
        } while(begin >= val);
        return val;
    }

    /**
     * @return the res
     */
    public static String getRes() {
        return res;
    }

    /**
     * @param aRes the res to set
     */
    private static void setRes(String aRes) {
        res = aRes;
    }

    private RandomNumber() {
    }
}
