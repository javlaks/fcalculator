package org.fc.algorithms;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * @author Javlak'S
 */
public class Password {
    public static String customPassword(String word, String length) {
        String password = "";

        for(int i = 0; i < Integer.valueOf(length); i++) {
            password += (word.charAt((int) (Math.random() * word.length())));
        }
        return password;
    }

    public static String randomPassword(String word, String length) {
        String password = "";

        for(int i = 0; i < Integer.valueOf(length); i++) {
            password += (word.charAt((int) (Math.random() * word.length())));
        }
        return password;
    }

    public static String random(String word, String length) {
        return random(word, Integer.valueOf(length));
    }

    public static String random(String word, int length) {
        return RandomStringUtils.random(length, word);
    }

    private Password() {
    }
}
