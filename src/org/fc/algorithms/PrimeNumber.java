/*
 * @(#)PrimeNumber.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.algorithms;

/**
 * This class contains methods for calculate prime numbers
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 1.2
 * @version 4.0
 */
public class PrimeNumber {
    private String valor;

    //Este metodo sirve para saber si un numero ingresado por el usuario es primo o no
    public void saberSiEsPrimo(long primo) {
        if(primo == 1) {
            setValor("El número " + primo + " no es primo");
        } else if(primo < 4) {
            setValor("El número " + primo + " es primo");
        } else {
            long m = (long) Math.sqrt(primo);

            for(long i = 2; i <= m; i++) {
                if(primo % i == 0) {
                    setValor("El número " + primo + " no es primo");
                    return;
                }
            }
            setValor("El número " + primo + " es primo");
        }
    }

    //Este metodo calcula una determinada cantidad de numero y deduce cuales son primos
    public boolean esPrimo(long num) {

        if(num == 1) {
            return false;
        } else if(num < 4) {
            return true;
        }

        long m = (long) Math.sqrt(num);
        for(long i = 2; i <= m; i++) {
            if(num % i == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return the valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(String valor) {
        this.valor = valor;
    }
}
