/*
 * @(#)Age.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.algorithms;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.fc.utils.data.Constants;
import org.joda.time.Period;
import org.joda.time.PeriodType;

/**
 * This class contains the methods necesary for calculate the age in year, months and days
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 1.2
 * @version 5.0
 */
public class Age {
    private static int diaReal, mesReal, anioReal;
    private static int tempDia, tempMes, tempAnio;
    private static long horas, minutos, segundos;
    private String resultado;
    private static String val1, val2, val3;
    private String salto2 = Constants.salto2;

    /**
     * Dada una fecha en forma de Date, nos devuelve la fecha en forma dd/mm/aaaa
     *
     * @param date Fecha en forma Date
     *
     * @return La fecha en forma de String
     */
    public static String getDate(Date date) {
        String text;

        String dia, mes, anio;

        Calendar calendario = new GregorianCalendar();

        calendario.setTime(date);

        if(calendario.get(Calendar.DAY_OF_MONTH) < 10) {
            dia = "0" + calendario.get(Calendar.DAY_OF_MONTH);
        } else {
            dia = String.valueOf(calendario.get(Calendar.DAY_OF_MONTH));
        }

        if((calendario.get(Calendar.MONTH) + 1) < 10) {
            mes = "0" + (calendario.get(Calendar.MONTH) + 1);
        } else {
            mes = String.valueOf((calendario.get(Calendar.MONTH) + 1));
        }

        anio = String.valueOf(calendario.get(Calendar.YEAR));

        text = dia + "/" + mes + "/" + anio;

        return text;
    }

    /**
     * Dadas dos fechas en formato dd/mm/aaaa nos devolverá la edad exacta.
     *
     * @param edad Refiere de la fecha de nacimiento del alumno.
     * @param fecha Refiere de la fecha a la cual se va a sacar la diferencia.
     * @param temporal
     */
    public void calcularEdad(Date edad, Date fecha, int temporal) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(edad);

        Integer diaEdad, mesEdad, anioEdad;
        Integer diaFecha, mesFecha, anioFecha;

        diaEdad = Integer.valueOf(getDate(edad).substring(0, 2));
        mesEdad = Integer.valueOf(getDate(edad).substring(3, 5));
        anioEdad = Integer.valueOf(getDate(edad).substring(6, 10));

        diaFecha = Integer.valueOf(getDate(fecha).substring(0, 2));
        mesFecha = Integer.valueOf(getDate(fecha).substring(3, 5));
        anioFecha = Integer.valueOf(getDate(fecha).substring(6, 10));

        //calculando temporalmente los datos
        tempAnio = (anioFecha - anioEdad);
        tempMes = (mesFecha - mesEdad);
        tempDia = (diaFecha - diaEdad);

        if(tempMes > 0) {
            if(tempDia >= 0) {
                anioReal = tempAnio;
            } else {
                if(tempAnio > 0) {
                    anioReal = tempAnio;
                } else {
                    anioReal = tempAnio;
                }
            }
        } else if(tempMes == 0) {
            if(tempDia >= 0) {
                anioReal = tempAnio;
            } else {
                anioReal = tempAnio - 1;
            }
        } else if(tempMes < 0) {
            if(tempDia <= 0) {
                anioReal = tempAnio - 1;
            } else {
                if(tempAnio >= 0) {
                    anioReal = tempAnio - 1;
                } else {
                    anioReal = tempAnio;
                }
            }
        }

        //para los meses
        if(tempDia >= 0) {
            if(tempMes >= 0) {
                mesReal = Math.abs(tempMes);
            } else {
                mesReal = tempMes + 12;
            }
        } else {
            if(tempMes <= 0) {
                mesReal = tempMes + 11;
            } else {
                mesReal = Math.abs(tempMes) - 1;
            }
        }

        //para los dias
        if(tempDia >= 0) {
            diaReal = tempDia;
        } else {
            diaReal = temporal - Math.abs(tempDia);
        }

        //para las horas, minutos y segundos

        horas = ((anioReal * 365) * 24) + ((mesReal * 30) * 24) + (diaReal * 24);

        minutos = horas * 60;

        segundos = minutos * 60;

        //para el resultado

        val1 = "Han transcurrido " + anioReal + " años, " + mesReal + " meses y " + diaReal + " días.";

        val2 = salto2 + "ó " + horas + " horas" + " ó " + minutos + " minutos" + " ó " + segundos + " segundos";

        val3 = salto2 + "Por cierto, era " + getDay(cal.get(Calendar.DAY_OF_WEEK)) + " cuando naciste";

        if(anioReal >= 0) {
            setResultado(val1 + val2 + val3);
        } else {
            setResultado("Revisa la fecha de nacimiento, parece ser que está incorrecta." + salto2 + "La fecha de nacimiento no puede ser mayor que la actual.");
        }
    }

    /**
     * @return the resultado
     */
    public String getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    private void setResultado(String resultado) {
        this.resultado = resultado;
    }

    /**
     * Este metodo permite obtener el dia de la semana.
     *
     * @param day Entero referente al dia segun las variables de Calendar
     *
     * @return Una cadena con el nombre del dia.
     */
    private String getDay(int day) {
        switch(day) {
            case 1:
                return "Domingo";
            case 2:
                return "Lunes";
            case 3:
                return "Martes";
            case 4:
                return "Miércoles";
            case 5:
                return "Jueves";
            case 6:
                return "Viernes";
            case 7:
                return "Sábado";
            default:
                return "No existe el dia";
        }
    }

    public String calculateDifference(Calendar date1, Calendar date2) {
        Period p2 = new Period(date1.getTimeInMillis(), date2.getTimeInMillis(), PeriodType.standard());

        StringBuilder sb = new StringBuilder();
        sb.append(getBeginMsg(date2));
        sb.append(p2.getYears()).append(p2.getYears() != 1 ? " años, " : " año, ");
        sb.append(p2.getMonths()).append(p2.getMonths() != 1 ? " meses, " : " mes, ");
        sb.append(p2.getWeeks()).append(p2.getWeeks() != 1 ? " semanas, " : " semana, ");
        sb.append(p2.getDays()).append(p2.getDays() != 1 ? " días, " : " día, ");
        sb.append(p2.getHours()).append(p2.getHours() != 1 ? " horas, y " : " hora, y ");
        sb.append(p2.getMinutes()).append(p2.getMinutes() != 1 ? " minutos." : " minuto.");
        sb.append("\n\n");
        sb.append(getEndMsg(date1, date2));

        return sb.toString();
    }
    
    private String getBeginMsg(Calendar calendar){
        Calendar cal = new GregorianCalendar();
        int val = cal.compareTo(calendar);
        if(val > 0){
           return "Han transcurrido ";
        } else {
            return "Transcurrirán ";
        }
    }
    
    private String getEndMsg(Calendar calendar1, Calendar calendar2){
        Calendar cal = new GregorianCalendar();
        int val = cal.compareTo(calendar2);
        if(val > 0){
           return "Por cierto, era " + getDay(calendar1.get(Calendar.DAY_OF_WEEK));
        } else {
            return "Por cierto, será " + getDay(calendar2.get(Calendar.DAY_OF_WEEK));
        }
    }
}
