/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import org.fc.utils.Utilities;
import org.fc.utils.configuration.Configuration;
import org.fc.utils.gui.CloseWindowUI;
import vic.tor.pf.actions.lp.TaskList;
import vic.tor.pf.ui.VPlataform;

/**
 *
 * @author Galdino
 */
public class FCPlataform extends VPlataform {
    private static final long serialVersionUID = -2584097536302470179L;

    /**
     * Creates new form VPlataform
     *
     * @param title
     */
    public FCPlataform(String title) {
        this(title, null, null);
    }

    public FCPlataform(String title, JMenuBar menuBar) {
        this(title, menuBar, null);
    }

    public FCPlataform(String title, TaskList taskList) {
        this(title, null, taskList);
    }

    public FCPlataform(String title, final JMenuBar menuBar, final TaskList taskList) {
        super(title, menuBar, taskList);
        setIconImage(Utilities.getImage("/org/fc/files/images/icons/icono.png"));
        setSize(820, 520);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (dim.width - getSize().width) / 2;
        int y = (dim.height - getSize().height) / 2;
        setLocation(x, y);
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    @Override
    public void openApp() {
        showApp();
    }

    @Override
    protected void exit() {
        String x = Configuration.getPropiedad("cerrar");

        if("3".equals(x)) {
            if(Configuration.getPropiedadBoolean("mensajeCerrar")) {
                CloseWindowUI cerrar = new CloseWindowUI();
                cerrar.shows();
            } else {
                System.exit(0);
            }
        } else {
            dispose();
        }
    }
}
