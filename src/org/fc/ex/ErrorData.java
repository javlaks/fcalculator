/*
 * @(#)ErrorData.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.ex;

import java.util.logging.Level;
import org.jdesktop.swingx.error.ErrorInfo;

/**
 * This class contains a simple version of an ErrorInfo method.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.1
 * @version 4.1
 */
public class ErrorData {
    private ErrorInfo error;
    private String titulo = "Ha ocurrido un error.";
    private String subtitulo = "Es importante que lo reporte para corregir este fallo.";

    public ErrorData(Exception _ex) {
        error = new ErrorInfo(titulo, subtitulo, null, null, _ex, Level.ALL, null);
    }

    public ErrorData(Exception _ex, String _titulo) {
        error = new ErrorInfo(_titulo, subtitulo, null, null, _ex, Level.ALL, null);
    }

    public ErrorData(Exception _ex, String _titulo, String _subtitulo) {
        error = new ErrorInfo(_titulo, _subtitulo, null, null, _ex, Level.ALL, null);
    }

    public ErrorInfo getErrorInfo() {
        return error;
    }
}
