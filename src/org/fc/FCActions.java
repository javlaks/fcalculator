/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;
import org.fc.apps.calc.CalculatorUI;
import org.fc.apps.convert.ConvertUI;
import org.fc.apps.geom.GeoGUI;
import org.fc.apps.gui.calculador.AgeUI;
import org.fc.apps.gui.calculador.ErrorUI;
import org.fc.apps.gui.calculador.StatisticsUI;
import org.fc.apps.gui.extras.NumericalSystemUI;
import org.fc.apps.gui.extras.RuleOfThreeUI;
import org.fc.apps.gui.extras.WordCountUI;
import org.fc.apps.gui.formulas.GeneralFormulaUI;
import org.fc.apps.gui.formulas.GreekAlphabetUI;
import org.fc.apps.gui.generator.DictionaryUI;
import org.fc.apps.gui.generator.PasswordUI;
import org.fc.apps.gui.generator.PrimeNumberUI;
import org.fc.gui.AboutUI;
import org.fc.utils.Updates;
import org.fc.utils.Utilities;
import org.fc.utils.configuration.Configuration;
import org.fc.utils.gui.OptionsUI;
import org.fc.utils.net.Http;
import vic.tor.pf.actions.SimpleAction;
import vic.tor.pf.ui.VPanel;
import vic.tor.pf.ui.VPlataform;
import vic.tor.pf.util.Messageable;

/**
 *
 * @author Galdino
 */
public class FCActions {
    private static VPlataform plataform;

    public static void setPlataform(VPlataform _plataform) {
        plataform = _plataform;
    }

    /**
     * Manda llamar a la herramienta de AgeUI
     *
     * @return
     */
    public static SimpleAction edad() {
        return new SimpleAction("Fechas", "Aplicación para calcular diferencia de fechas.",
                Utilities.getImageIcon("/org/fc/files/images/icons/age.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new AgeUI("Fechas"));
            }
        };
    }

    /**
     * Manda llamar a la herramienta de ErrorUI
     *
     * @return
     */
    public static SimpleAction error() {
        return new SimpleAction("Error", "Aplicación para calcular error relativo y absoluto.",
                Utilities.getImageIcon("/org/fc/files/images/icons/error.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new ErrorUI("Error Relativo y Absoluto"));
            }
        };
    }

    /**
     * Manda llamar a la herramienta de Estadistica
     *
     * @return
     */
    public static SimpleAction estadistica() {
        return new SimpleAction("Estadística", "Aplicación para cálculo estadístico.",
                Utilities.getImageIcon("/org/fc/files/images/icons/statistics.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new StatisticsUI("Estadística"));
            }
        };
    }

    /**
     * Manda llamar a la herramienta de Formula General
     *
     * @return
     */
    public static SimpleAction formulaGeneral() {
        return new SimpleAction("Fórmula General", "Aplicación para calcular variables mediante la fórmula general.",
                Utilities.getImageIcon("/org/fc/files/images/icons/formula.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new GeneralFormulaUI("Fórmula General"));
            }
        };
    }

    /**
     * Manda llamar a la herramienta de GeoGUI
     *
     * @return
     */
    public static SimpleAction geometria() {
        return new SimpleAction("Geometría", "Aplicación para el cálculo de figuras geométricas.",
                Utilities.getImageIcon("/org/fc/files/images/icons/geometry.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                new GeoGUI().shows();
            }
        };
    }

    /**
     * Manda llamar a la herramienta de Numeros PrimeNumberUI
     *
     * @return
     */
    public static SimpleAction numerosPrimos() {
        return new SimpleAction("Números Primos", "Aplicación para generar números primos.",
                Utilities.getImageIcon("/org/fc/files/images/icons/prime.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new PrimeNumberUI("Números Primos"));
            }
        };
    }

    /**
     * Manda llamar a la herramienta de Regla De 3
     *
     * @return
     */
    public static SimpleAction reglaDe3() {
        return new SimpleAction("Regla de 3", "Aplicación para calcular mediante la regla de 3.",
                Utilities.getImageIcon("/org/fc/files/images/icons/3rule.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new RuleOfThreeUI("Regla de 3"));
            }
        };
    }

    /**
     * Manda llamar a la herramienta de Sistema Numerico
     *
     * @return
     */
    public static SimpleAction sistemaNumerico() {
        return new SimpleAction("Sistema Numérico", "Aplicación para convertir entre diferentes sistemas numéricos.",
                Utilities.getImageIcon("/org/fc/files/images/icons/numericalsystem.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new NumericalSystemUI("Sistema Numérico"));
            }
        };
    }

    public static SimpleAction calculadora() {
        return new SimpleAction("Calculadora", "Aplicación calculadora sencilla.",
                Utilities.getImageIcon("/org/fc/files/images/icons/calc.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                new CalculatorUI().shows();
            }
        };
    }

    public static SimpleAction conversor() {
        return new SimpleAction("Conversor", "Aplicación para convertir entre diferentes unidades.",
                Utilities.getImageIcon("/org/fc/files/images/icons/converter.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new ConvertUI("Conversor"));
            }
        };
    }

    public static SimpleAction agriego() {
        return new SimpleAction("Alfabeto Griego", "Aplicación que muestra una lista con el alfabeto griego.",
                Utilities.getImageIcon("/org/fc/files/images/icons/omega.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new GreekAlphabetUI("Alfabeto Griego"));
            }
        };
    }

    public static SimpleAction passwordG() {
        return new SimpleAction("Contraseñas", "Aplicación para generar contraseñas.",
                Utilities.getImageIcon("/org/fc/files/images/icons/password.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new PasswordUI("Contraseñas"));
            }
        };
    }

    /**
     * Manda llamar a la herramienta de Numeros PrimeNumberUI
     *
     * @return
     */
    public static SimpleAction dictionary() {
        return new SimpleAction("Diccionario", "Aplicación para generar diccionario de palabras (Permutaciones).",
                Utilities.getImageIcon("/org/fc/files/images/icons/dictionary.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new DictionaryUI("Diccionario"));
            }
        };
    }

    /**
     * Manda llamar a la herramienta de Numeros PrimeNumberUI
     *
     * @return
     */
    public static SimpleAction wordCount() {
        return new SimpleAction("Contador de Palabras", "Aplicación para contar caracteres y palabras.",
                Utilities.getImageIcon("/org/fc/files/images/icons/wordcount.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new WordCountUI("Contador de Palabras"));
            }
        };
    }

    /////////// FIN ACCIONES APPS //////////
    /////////// Otras acciones /////////
    public static SimpleAction exit() {
        SimpleAction s = new SimpleAction("Salir", "Terminar la ejecución del FCalculator.",
                Utilities.getImageIcon("/org/fc/files/images/icons/exit.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                System.exit(0);
            }
        };
        s.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
        return s;
    }

    public static SimpleAction about() {
        SimpleAction s = new SimpleAction("Acerca de", "Información acerca de FCalculator.",
                Utilities.getImageIcon("/org/fc/files/images/icons/about.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new AboutUI("Acerca de FCalculator", AboutUI.ABOUT));
            }
        };
        s.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, InputEvent.CTRL_MASK));
        return s;
    }

    public static SimpleAction checkUpdates(final Messageable transmiter) {
        return new SimpleAction("Revisar Actualizaciones", "Revisa actualizaciones a través de Internet.",
                Utilities.getImageIcon("/org/fc/files/images/icons/updates.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                Updates.check(transmiter);
            }
        };
    }

    public static SimpleAction options() {
        SimpleAction s = new SimpleAction("Opciones", "Abre la ventana de opciones y configuración.",
                Utilities.getImageIcon("/org/fc/files/images/icons/options.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                new OptionsUI().shows();
            }
        };
        s.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
        return s;
    }

    public static SimpleAction web(final String word) {
        return new SimpleAction(word, "Abre el navegador en " + word,
                Utilities.getImageIcon("/org/fc/files/images/icons/web.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                if(word.equals("Blog")) {
                    Http.openURL(Utilities.BLOG_URL);
                } else {
                    Http.openURL(Utilities.FCALCULATOR_URL);
                }
            }
        };
    }

    public static SimpleAction changelog() {
        return new SimpleAction("Historial", "Muestra el historial de cambios.",
                Utilities.getImageIcon("/org/fc/files/images/icons/changes.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new AboutUI("Acerca de FCalculator", AboutUI.HISTORY));
//                new AboutUI().shows(3);
            }
        };
    }

    public static SimpleAction license() {
        return new SimpleAction("Licencias", "Muestra las licencias de distribución de FCalculator y Librerias.",
                Utilities.getImageIcon("/org/fc/files/images/icons/gpl.png")) {
            private static final long serialVersionUID = -2705495332740995775L;

            @Override
            public void actionPerformed(ActionEvent evt) {
                showApp(new AboutUI("Acerca de FCalculator", AboutUI.LICENSES));
            }
        };
    }

    private static void showApp(VPanel panel) {
        boolean in_panel = Configuration.getPropiedadBoolean("en_panel");
        if(in_panel && plataform != null) {
            if(!plataform.isShowingApp()) {
                panel.showInFrame(plataform);
            } else {
                plataform.setVPanel(panel);
            }
        } else {
            panel.showInFrame(plataform);
        }
    }

    private FCActions() {
    }
}
