/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc;

import java.awt.Image;
import javax.swing.JMenuBar;
import org.fc.algorithms.RandomNumber;
import org.fc.gui.FCMainPanel;
import org.fc.tray.FCTrayMenu;
import org.fc.utils.JavaVersion;
import org.fc.utils.Updates;
import org.fc.utils.Utilities;
import org.fc.utils.configuration.Configuration;
import vic.tor.pf.actions.lp.TaskList;
import vic.tor.pf.tray.Tray;
import vic.tor.pf.ui.VSplash;
import vic.tor.pf.util.ZThreads;

/**
 *
 * @author Galdino
 */
class FCSplash extends VSplash {
    FCSplash(Image image) {
        super(image);
    }

    @Override
    protected Void doInBackground() throws Exception {

        show();
        JavaVersion.check();
        publish(RandomNumber.nextRandom(5, 10));
        ZThreads.sleep(200);

        FCTrayMenu trayMenu = new FCTrayMenu();
        final Tray tray = new Tray("FCalculator",
                Utilities.getImage("/org/fc/files/images/icons/icono.png"), trayMenu);
        trayMenu.setTransmiter(tray);
        publish(RandomNumber.nextRandom(15, 30));
        ZThreads.sleep(200);

        ZThreads.runSwingThread(new Runnable() {
            @Override
            public void run() {
                JMenuBar m = new FCMenuBar();
                TaskList t = new FCTaskList().getTaskList();
                FCPlataform plataform = new FCPlataform("FCalculator", m, t);
                tray.setApplication(plataform);
                plataform.setVPanel(new FCMainPanel("Panel Principal"));
                FCActions.setPlataform(plataform);
                if(!Configuration.getPropiedadBoolean("minimize")) {
                    plataform.openApp();
                }
            }
        }, true);

        publish(RandomNumber.nextRandom(40, 85));
        ZThreads.sleep(400);

        tray.show();
        if(Configuration.getPropiedadBoolean("casillaBienvenida")) {
            tray.showMessage("FCalculator", "Bienvenido");
        }
        publish(100);
        ZThreads.sleep(500);

        if(Configuration.getPropiedadBoolean("update")) {
            Updates.check(tray);
        }
        return null;
    }
}
