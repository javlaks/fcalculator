/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc.gui;

import java.awt.BorderLayout;
import org.fc.utils.Utilities;
import org.jdesktop.swingx.JXLabel;

/**
 *
 * @author Galdino
 */
public class FCMainPanel extends FCVPanel {
    private static final long serialVersionUID = 3137898822175669708L;

    public FCMainPanel(String title) {
        super(title);
        initComponents();
    }

    private void initComponents() {
        JXLabel l = new JXLabel(Utilities.getImageIcon("/org/fc/files/images/walls/fc_wall.png"));
        setLayout(new BorderLayout());
        add(l, BorderLayout.CENTER);
    }
}
