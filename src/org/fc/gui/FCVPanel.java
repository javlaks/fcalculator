/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc.gui;

import java.awt.Component;
import java.awt.Window;
import javax.swing.JOptionPane;
import org.fc.utils.Utilities;
import org.fc.utils.configuration.CheckOptions;
import org.jdesktop.swingx.JXFrame;
import vic.tor.pf.ui.VPanel;

/**
 *
 * @author Galdino
 */
public class FCVPanel extends VPanel {
    private static final long serialVersionUID = -2997872219284090167L;
    private JXFrame f;

    /**
     *
     * @param title
     */
    public FCVPanel(String title) {
        super(title);
    }

    @Override
    public void showInFrame(Window window) {
        f = getInFrame();
        f.setLocationRelativeTo(window);
        f.setVisible(true);
        f.setIconImage(Utilities.getImage("/org/fc/files/images/icons/icono.png"));
    }

    protected void checkOptions() {
        CheckOptions.start();
    }

    @Override
    protected void showErrorMessage(String title, String message) {
        Component c = f != null ? f : this;
        JOptionPane.showMessageDialog(c, message, title, JOptionPane.ERROR_MESSAGE);
    }

    @Override
    protected void showInfoMessage(String title, String message) {
        Component c = f != null ? f : this;
        JOptionPane.showMessageDialog(c, message, title, JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    protected int showConfirmYesNoMessage(String title, String message) {
        Component c = f != null ? f : this;
        return JOptionPane.showConfirmDialog(c, message, title, JOptionPane.YES_NO_OPTION);
    }
}
