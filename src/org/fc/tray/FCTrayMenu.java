/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc.tray;

import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import org.fc.FCActions;
import vic.tor.pf.tray.TrayPopup;
import vic.tor.pf.util.Messageable;

/**
 *
 * @author Galdino
 */
public class FCTrayMenu extends TrayPopup {
    private static final long serialVersionUID = 1315386368425526787L;
    private Messageable transmiter;

    public FCTrayMenu() {
        // Create a popup menu components
        aboutItem = new MenuItem("Acerca de");
        actualizaciones = new MenuItem("Revisar Actualizaciones");

        //Utilidades
        Menu utilidadesMenu = new Menu("Utilidades");

        //Menus calculador
        Menu calculadorMenu = new Menu("Calculador");
        edadMenu = new MenuItem("Fechas");
        errorMenu = new MenuItem("Error");
        estadisticaMenu = new MenuItem("Estadística");
        geometriaMenu = new MenuItem("Geometría");

        //Menus Extras
        Menu extrasMenu = new Menu("Extras");
        calculadoraMenu = new MenuItem("Calculadora");
        conversorMenu = new MenuItem("Conversor");
        reglade3Menu = new MenuItem("Regla de 3");
        sistemaNumericoMenu = new MenuItem("Sistema Numérico");
        wordCountMenu = new MenuItem("Contador de Palabras");

        //Menus Fórmulas y Tablas
        Menu formytabMenu = new Menu("Form. y Tab.");
        alfabetoMenu = new MenuItem("Alfabeto Griego");
        formulagralMenu = new MenuItem("Fórmula Gral.");

        //Menus Generador
        Menu generatorMenu = new Menu("Generador");
        passwordGMenu = new MenuItem("Contraseñas");
        dictionaryMenu = new MenuItem("Diccionario");
        primeGMenu = new MenuItem("Números Primos");

        //Otros Menus
        exitItem = new MenuItem("Salir");

        //Primer bloque
        add(actualizaciones);
        addSeparator();
        //Bloque de utilidades
        add(utilidadesMenu);
        addSeparator();
        //Calculador
        utilidadesMenu.add(calculadorMenu);
        calculadorMenu.add(edadMenu);
        calculadorMenu.add(errorMenu);
        calculadorMenu.add(estadisticaMenu);
        calculadorMenu.add(geometriaMenu);
        //Extras
        utilidadesMenu.add(extrasMenu);
        extrasMenu.add(calculadoraMenu);
        extrasMenu.add(wordCountMenu);
        extrasMenu.add(conversorMenu);
        extrasMenu.add(reglade3Menu);
        extrasMenu.add(sistemaNumericoMenu);
        //Fórmulas
        utilidadesMenu.add(formytabMenu);
        formytabMenu.add(alfabetoMenu);
        formytabMenu.add(formulagralMenu);
        //Generador
        utilidadesMenu.add(generatorMenu);
        generatorMenu.add(passwordGMenu);
        generatorMenu.add(dictionaryMenu);
        generatorMenu.add(primeGMenu);
        //Último bloque
        add(aboutItem);
        add(exitItem);

        aboutItem.addActionListener(this);
        actualizaciones.addActionListener(this);
        exitItem.addActionListener(this);
        edadMenu.addActionListener(this);
        errorMenu.addActionListener(this);
        estadisticaMenu.addActionListener(this);
        formulagralMenu.addActionListener(this);
        geometriaMenu.addActionListener(this);
        calculadoraMenu.addActionListener(this);
        conversorMenu.addActionListener(this);
        reglade3Menu.addActionListener(this);
        sistemaNumericoMenu.addActionListener(this);
        alfabetoMenu.addActionListener(this);
        passwordGMenu.addActionListener(this);
        primeGMenu.addActionListener(this);
        dictionaryMenu.addActionListener(this);
        wordCountMenu.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(aboutItem)) {
            FCActions.about().actionPerformed(e);
        } else if(e.getSource().equals(actualizaciones)) {
            FCActions.checkUpdates(transmiter).actionPerformed(e);
        } else if(e.getSource().equals(exitItem)) {
            FCActions.exit().actionPerformed(e);
        } else if(e.getSource().equals(edadMenu)) {
            FCActions.edad().actionPerformed(e);
        } else if(e.getSource().equals(errorMenu)) {
            FCActions.error().actionPerformed(e);
        } else if(e.getSource().equals(estadisticaMenu)) {
            FCActions.estadistica().actionPerformed(e);
        } else if(e.getSource().equals(formulagralMenu)) {
            FCActions.formulaGeneral().actionPerformed(e);
        } else if(e.getSource().equals(geometriaMenu)) {
            FCActions.geometria().actionPerformed(e);
        } else if(e.getSource().equals(primeGMenu)) {
            FCActions.numerosPrimos().actionPerformed(e);
        } else if(e.getSource().equals(calculadoraMenu)) {
            FCActions.calculadora().actionPerformed(e);
        } else if(e.getSource().equals(conversorMenu)) {
            FCActions.conversor().actionPerformed(e);
        } else if(e.getSource().equals(reglade3Menu)) {
            FCActions.reglaDe3().actionPerformed(e);
        } else if(e.getSource().equals(sistemaNumericoMenu)) {
            FCActions.sistemaNumerico().actionPerformed(e);
        } else if(e.getSource().equals(alfabetoMenu)) {
            FCActions.agriego().actionPerformed(e);
        } else if(e.getSource().equals(passwordGMenu)) {
            FCActions.passwordG().actionPerformed(e);
        } else if(e.getSource().equals(dictionaryMenu)) {
            FCActions.dictionary().actionPerformed(e);
        } else if(e.getSource().equals(wordCountMenu)) {
            FCActions.wordCount().actionPerformed(e);
        }
    }

    public void setTransmiter(Messageable messageable) {
        transmiter = messageable;
    }

    private MenuItem aboutItem;
    private MenuItem actualizaciones;
    private MenuItem exitItem;
    private MenuItem edadMenu;
    private MenuItem errorMenu;
    private MenuItem estadisticaMenu;
    private MenuItem formulagralMenu;
    private MenuItem geometriaMenu;
    private MenuItem calculadoraMenu;
    private MenuItem conversorMenu;
    private MenuItem reglade3Menu;
    private MenuItem sistemaNumericoMenu;
    private MenuItem alfabetoMenu;
    private MenuItem passwordGMenu;
    private MenuItem primeGMenu;
    private MenuItem dictionaryMenu;
    private MenuItem wordCountMenu;

    @Override
    public void itemStateChanged(ItemEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
