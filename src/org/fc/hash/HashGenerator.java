/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc.hash;

import com.google.common.hash.Hashing;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Victor
 */
public enum HashGenerator {
    ADLER32("Adler-32"),
    CRC32("CRC 32"),
    MD2("MD2"),
    MD5("MD5"),
    MURMUR3_32("Murmur3_32"),
    MURMUR3_128("Murmur3_128"),
    SHA1("SHA-1"),
    SHA256("SHA-256"),
    SHA384("SHA-384"),
    SHA512("SHA-512"),
    SIPHASH24("SipHash 24");

    final String name;

    HashGenerator(String name) {
        this.name = name;
    }

    public static String getAdler32Text(byte[] text) {
        return Hashing.adler32().hashBytes(text).toString();
    }

    public static String getCrc32Text(byte[] text) {
        return Hashing.crc32().hashBytes(text).toString();
    }
    
    public static String getMD2Text(byte[] text) {
        return getStringMessageDigest(text, MD2.name());
    }

    public static String getMD5Text(byte[] text) {
        return Hashing.md5().hashBytes(text).toString();
    }
    
    public static String getMurmur3_32Text(byte[] text) {
        return Hashing.murmur3_32().hashBytes(text).toString();
    }

    public static String getMurmur3_128Text(byte[] text) {
        return Hashing.murmur3_128().hashBytes(text).toString();
    }

    public static String getSha1Text(byte[] text) {
        return Hashing.sha1().hashBytes(text).toString();
    }

    public static String getSha256Text(byte[] text) {
        return Hashing.sha256().hashBytes(text).toString();
    }

    public static String getSha384Text(byte[] text) {
        return getStringMessageDigest(text, SHA384.name());
    }
    
    public static String getSha512Text(byte[] text) {
        return Hashing.sha512().hashBytes(text).toString();
    }

    public static String getSipHash24Text(byte[] text) {
        return Hashing.sipHash24().hashBytes(text).toString();
    }
    
    /***
     * Convierte un arreglo de bytes a String usando valores hexadecimales
     * @param digest arreglo de bytes a convertir
     * @return String creado a partir de <code>digest</code>
     */
    private static String toHexadecimal(byte[] digest){
        String hash = "";
        for(byte aux : digest) {
            int b = aux & 0xff;
            if (Integer.toHexString(b).length() == 1) {
                hash += "0";
            }
            hash += Integer.toHexString(b);
        }
        return hash;
    }

    /***
     * Encripta un mensaje de texto mediante algoritmo de resumen de mensaje.
     * @param message texto a encriptar
     * @param algorithm algoritmo de encriptacion, puede ser: MD2, MD5, SHA-1, SHA-256, SHA-384, SHA-512
     * @return mensaje encriptado
     */
    public static String getStringMessageDigest(byte[] message, String algorithm){
        byte[] digest = null;
        byte[] buffer = message;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            messageDigest.reset();
            messageDigest.update(buffer);
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return toHexadecimal(digest);
    } 

}
