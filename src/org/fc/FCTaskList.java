/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc;

import org.jdesktop.swingx.JXTaskPane;
import vic.tor.pf.actions.lp.TaskList;

/**
 *
 * @author Galdino
 */
public class FCTaskList {
    private JXTaskPane calculador;
    private JXTaskPane extras;
    private JXTaskPane formulas;
    private JXTaskPane generador;
    private TaskList tl;

    public FCTaskList() {
        calculador = new JXTaskPane("Calculador");
        extras = new JXTaskPane("Extras");
        formulas = new JXTaskPane("Fórmulas y Tablas");
        generador = new JXTaskPane("Generador");

        calculador.add(FCActions.edad());
        calculador.add(FCActions.error());
        calculador.add(FCActions.estadistica());
        calculador.add(FCActions.geometria());

        extras.add(FCActions.calculadora());
        extras.add(FCActions.wordCount());
        extras.add(FCActions.conversor());
        extras.add(FCActions.reglaDe3());
        extras.add(FCActions.sistemaNumerico());

        formulas.add(FCActions.agriego());
        formulas.add(FCActions.formulaGeneral());

        generador.add(FCActions.passwordG());
        generador.add(FCActions.dictionary());
        generador.add(FCActions.numerosPrimos());


        tl = new TaskList(calculador);
        tl.addTaks(extras);
        tl.addTaks(formulas);
        tl.addTaks(generador);

    }

    public TaskList getTaskList() {
        return tl;
    }
}
