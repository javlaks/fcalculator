/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc;

import javax.swing.JMenu;
import javax.swing.JMenuBar;

/**
 *
 * @author Galdino
 */
public class FCMenuBar extends JMenuBar {
    private static final long serialVersionUID = -4112092182577149114L;

    public FCMenuBar() {
        //Menus Principales
        menuArchivo = new JMenu("Archivo");
        menuUtilidades = new JMenu("Utilidades");
        menuHerramientas = new JMenu("Herramientas");
        menuAyuda = new JMenu("Ayuda");
        //Menus Utilidades
        menuCalculador = new JMenu("Calculador");
        menuExtras = new JMenu("Extras");
        menuFormYTab = new JMenu("Fórmulas y Tablas");
        menuGenerador = new JMenu("Generador");

        //Menu archivo
        menuArchivo.add(FCActions.exit());
        /////////Menu Utilidades
        //Menu Calculador
        menuCalculador.add(FCActions.edad());
        menuCalculador.add(FCActions.error());
        menuCalculador.add(FCActions.estadistica());
        menuCalculador.add(FCActions.geometria());
        //Menu Extras
        menuExtras.add(FCActions.calculadora());
        menuExtras.add(FCActions.wordCount());
        menuExtras.add(FCActions.conversor());
        menuExtras.add(FCActions.reglaDe3());
        menuExtras.add(FCActions.sistemaNumerico());
        //Menu Formulas y Tablas
        menuFormYTab.add(FCActions.agriego());
        menuFormYTab.add(FCActions.formulaGeneral());
        //Menu Generador
        menuGenerador.add(FCActions.passwordG());
        menuGenerador.add(FCActions.dictionary());
        menuGenerador.add(FCActions.numerosPrimos());
        //Menu Herramientas
        menuHerramientas.add(FCActions.options());
        //Menu Ayuda

        menuAyuda.add(FCActions.web("Blog"));
        menuAyuda.add(FCActions.web("Web"));
        menuAyuda.addSeparator();
        menuAyuda.add(FCActions.changelog());
        menuAyuda.add(FCActions.license());
        menuAyuda.addSeparator();
        menuAyuda.add(FCActions.about());

        //Adicion de Menus de Utilidades
        menuUtilidades.add(menuCalculador);
        menuUtilidades.add(menuExtras);
        menuUtilidades.add(menuFormYTab);
        menuUtilidades.add(menuGenerador);

        //Adición de menús
        add(menuArchivo);
        add(menuUtilidades);
        add(menuHerramientas);
        add(menuAyuda);
    }
    //Declaracion de componentes de los menus
    private JMenu menuUtilidades;
    private JMenu menuArchivo;
    private JMenu menuAyuda;
    private JMenu menuCalculador;
    private JMenu menuExtras;
    private JMenu menuFormYTab;
    private JMenu menuGenerador;
    private JMenu menuHerramientas;
}
