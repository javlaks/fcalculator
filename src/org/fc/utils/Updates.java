/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc.utils;

import org.fc.utils.net.VersionCheck;
import vic.tor.pf.util.Messageable;
import vic.tor.pf.util.Messageable.MessageTypes;

/**
 *
 * @author Galdino
 */
public class Updates {
    public static void check(Messageable transmiter) {
        switch(VersionCheck.checkNuevaVersion()) {
            case 0:
                transmiter.showMessage("FCalculator", "No se han comprobado nuevas actualizaciones.\n\nHaz clic para revisar si existen actualizaciones.", MessageTypes.NONE);
                break;
            case 1:
                transmiter.showMessage("FCalculator", "Es necesario actualizar FCalculator.\n\nHaz clic para descargar la nueva versión.", MessageTypes.WARNING);
                break;
            case 2:
                transmiter.showMessage("FCalculator", "No se puede comprobar si existen actualizaciones.\n\nRevisa tu conexión a Internet.", MessageTypes.ERROR);
                break;
            case 3:
                transmiter.showMessage("FCalculator", "FCalculator se encuentra actualizado.", MessageTypes.INFO);
                break;
        }
    }

    private Updates() {
    }
}
