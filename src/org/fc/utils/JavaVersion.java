/*
 * @(#)JavaVersion.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils;

import javax.swing.JOptionPane;
import org.apache.commons.lang3.SystemUtils;
import org.fc.utils.net.Http;

/**
 * This class contains methods to check the java version installed on the system. If the JVM is
 * older, show a message to update.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 2.1
 * @version 3.0
 */
public class JavaVersion {
    private static String link = "http://www.java.com";

    public static void check() {
        if(!SystemUtils.IS_JAVA_1_7) {
            errorJava();
        } 
    }

    private static void errorJava() {
        JOptionPane.showMessageDialog(null, "Versión Java Antigua", "Es necesario actualizar su versión de Java", JOptionPane.ERROR_MESSAGE);

        Http.openURL(link);

        System.exit(0);
    }

    private JavaVersion() {
    }
}
