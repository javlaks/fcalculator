/*
 * @(#)LooksAndFeels.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.look;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.fc.ex.ErrorData;
import org.fc.utils.configuration.Configuration;
import org.jdesktop.swingx.JXErrorPane;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.pushingpixels.substance.api.skin.SkinInfo;

/**
 * This class contains methods to manager the lookandfeel.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public class LooksAndFeels {
    private HashMap<String, Skin> looks;
    private SkinInfo skinInfo;
    private Skin skin;
    private Iterator<Skin> skinEntrys;

    public LooksAndFeels() {
        looks = new HashMap<>();
        fillMap();
    }

    public void setLook(final String lookName) {
        try {
            UIManager.setLookAndFeel(getLookClassPath(lookName));
        } catch(ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            try {
                Configuration.setPropiedad("lookandfeel", "Java Nimbus");
                Configuration.guardar();
                setLook("Java Nimbus");
            } catch(Exception ex1) {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch(ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex2) {
                    JXErrorPane.showDialog(null, new ErrorData(ex2).getErrorInfo());
                    System.exit(0);
                }
            }
        }
    }

    public ArrayList<String> getLookAndFeelInstalled() {
        ArrayList<String> list = new ArrayList<>();

        skinEntrys = getLooksNamesList();

        while(skinEntrys.hasNext()) {
            skin = skinEntrys.next();

            list.add(skin.getLookName());
        }

        return list;
    }

    private String getLookClassPath(String lookName) {
        Skin classn = looks.get(lookName);

        return classn.getLookClassName();
    }

    private void fillMap() {
        for(UIManager.LookAndFeelInfo laf : UIManager.getInstalledLookAndFeels()) {
            skin = new Skin("Java " + laf.getName(), laf.getClassName());

            looks.put("Java " + laf.getName(), skin);
        }

        Iterator<SkinInfo> it = getSubstanceLooksInfo();

        String temp;

        while(it.hasNext()) {
            skinInfo = it.next();

            temp = skinInfo.getClassName().substring(0, (skinInfo.getClassName().lastIndexOf('.') + 1));

            temp += "Substance";

            temp += skinInfo.getClassName().substring(skinInfo.getClassName().lastIndexOf('.') + 1, skinInfo.getClassName().lastIndexOf("Skin"));

            temp += "LookAndFeel";

            skin = new Skin("Substance " + skinInfo.getDisplayName(), temp);

            looks.put("Substance " + skinInfo.getDisplayName(), skin);
        }
    }

    private Iterator<SkinInfo> getSubstanceLooksInfo() {
        ArrayList<SkinInfo> list = new ArrayList<>(SubstanceLookAndFeel.getAllSkins().values());

        return list.iterator();
    }

    private Iterator<Skin> getLooksNamesList() {
        ArrayList<Skin> list = new ArrayList<>(looks.values());

        return list.iterator();
    }
}
