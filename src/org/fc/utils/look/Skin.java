/*
 * @(#)Skin.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.look;

/**
 * This class contains methods and constants for manager the skin class for the lookandfeel
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public class Skin {
    private String lookName;
    private String lookClassName;

    /**
     *
     * @param lookName
     * @param lookClassName
     */
    public Skin(String lookName, String lookClassName) {
        this.lookName = lookName;
        this.lookClassName = lookClassName;
    }

    /**
     * @return the lookName
     */
    public String getLookName() {
        return lookName;
    }

    /**
     * @param lookName the lookName to set
     */
    public void setLookName(String lookName) {
        this.lookName = lookName;
    }

    /**
     * @return the lookClassName
     */
    public String getLookClassName() {
        return lookClassName;
    }

    /**
     * @param lookClassName the lookClassName to set
     */
    public void setLookClassName(String lookClassName) {
        this.lookClassName = lookClassName;
    }
}
