/*
 * @(#)Files.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils;

import com.google.common.base.Charsets;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class contains methods for read and write files.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.1
 */
public class Files {
    public String getText(String file, String errorMessage) {
        try {
            return com.google.common.io.Files.toString(new File(file), Charsets.UTF_8);
        } catch(IOException ex) {
            return errorMessage != null ? errorMessage : ex.getMessage();
        }
    }

    public String getText(String file) {
        return getText(file, null);
    }

    public String getText(InputStream file) {
        return getText(file, null);
    }

    public String getText(InputStream file, String errorMessage) {
        InputStreamReader isr;
        try {
            StringBuilder buffer = new StringBuilder();
            isr = new InputStreamReader(file, "UTF8");
            try(Reader in = new BufferedReader(isr)) {
                int ch;
                while((ch = in.read()) > -1) {
                    buffer.append((char) ch);
                }
            }
            file.close();
            isr.close();

            return buffer.toString();
        } catch(IOException ex) {
            return errorMessage != null ? errorMessage : ex.getMessage();
        }
    }

    public void printText(String file, String text) {
        try {
            com.google.common.io.Files.write(text, new File(file), Charsets.UTF_8);
        } catch(IOException ex) {
            Logger.getLogger(Files.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
