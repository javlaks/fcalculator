/*
 * @(#)WinLocation.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * This class contains methods to location frames and dialog in center screen. And make visible.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 1.0
 * @version 1.1
 */
public class WinLocation {
    /**
     *
     * @param ventana
     */
    public static void setLocation(JFrame ventana) {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        int w = ventana.getSize().width;
        int h = ventana.getSize().height;
        int x = (dim.width - w) / 2;
        int y = (dim.height - h) / 2;

        // Coloca la venatana en la ubicacion determinada
        ventana.setLocation(x, y);
        ventana.setVisible(true);
    }

    /**
     *
     * @param ventana
     */
    public static void setLocation(JDialog ventana) {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        int w = ventana.getSize().width;
        int h = ventana.getSize().height;
        int x = (dim.width - w) / 2;
        int y = (dim.height - h) / 2;

        // Coloca la venatana en la ubicacion determinada
        ventana.setLocation(x, y);
        ventana.setVisible(true);
    }

    private WinLocation() {
    }
}
