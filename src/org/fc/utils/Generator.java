/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fc.utils;

/**
 *
 * @author Galdino
 */
public interface Generator {
    public void generate();

    public void cancel();

    public boolean isGenerating();
}
