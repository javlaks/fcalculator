/*
 * @(#)CheckConfiguration.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.configuration;

import java.io.File;
import java.util.ResourceBundle;
import org.fc.utils.Utilities;

/**
 * This class contains methods for manager the configuration
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public class CheckConfiguration {
    private static ResourceBundle recursoConversor = null;

    static {
        recursoConversor = ResourceBundle.getBundle("org/fc/files/properties/Version");
    }
    public final static int version = getVersion();
    public final static int major = getMajor();
    public final static int minor = getMinor();
    private File archivo = new File(Utilities.getOptionsFilePath());

    public void check() {
        try {
            int tempVersion = Integer.valueOf(Configuration.getPropiedad("version")).intValue();
            int tempMajor = Integer.valueOf(Configuration.getPropiedad("major")).intValue();
            int tempMinor = Integer.valueOf(Configuration.getPropiedad("minor")).intValue();

            revisar(tempVersion, tempMajor, tempMinor);
        } catch(NumberFormatException nfe) {
            reiniciar();
        }
    }

    private void revisar(int a, int b, int c) {
        if(a < version) {
            reiniciar();
        } else if(b < major) {
            reiniciar();
        } else if(c < minor) {
            reiniciar();
        }
    }

    private void reiniciar() {
        FileConfiguration lastConfiguration = new FileConfiguration();

        lastConfiguration.readProperties(Configuration.getPropiedades());

        if(archivo.delete()) {
            Configuration.setDefaults();
            Configuration.setOpc(lastConfiguration.getEntries());
        }
    }

    public static int getVersion() {
        return Integer.valueOf(recursoConversor.getString("version"));
    }

    public static int getMajor() {
        return Integer.valueOf(recursoConversor.getString("major"));
    }

    public static int getMinor() {
        return Integer.valueOf(recursoConversor.getString("minor"));
    }

    public static ResourceBundle getResources() {
        return recursoConversor;
    }
}
