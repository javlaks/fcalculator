/*
 * @(#)CheckOptions.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.configuration;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Toolkit;
import java.net.URL;

/**
 * This class contains methods for manager the configuration
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public class CheckOptions {
    private static CheckOptions cop = new CheckOptions();
    private static AudioClip ac;
    private static URL url;
    /**
     * Lista que contiene todos los sonidos disponibles
     */
    private final String lista[] = {
        "Aqua", "Beep", "Blub", "Chimes", "Chord", "Ding", "Notify",
        "Pop", "Start", "Telephone", "Whish"
    };

    /**
     * Metodo para comprobar si se deben de mostrar los beeps y mensajes de error en caso de
     * producirse un error
     */
    public static void start() {
        cop.beeps();
    }

    private void beeps() {
        String sonido;
        sonido = Configuration.getPropiedad("sonido");

        if(Configuration.getPropiedadBoolean("casillaSonidos")) {
            playSound(sonido);
        }
    }

    public synchronized void playSound(String sonido) {

        if(sonido.equals("Beep")) {
            Toolkit.getDefaultToolkit().beep();
        } else {
            url = this.getClass().getResource("/org/fc/files/sounds/" + sonido + ".wav");
            ac = Applet.newAudioClip(url);
            ac.play();
        }
    }

    /**
     * @return the lista
     */
    public String[] getLista() {
        return lista;
    }
}
