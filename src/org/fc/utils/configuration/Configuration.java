/*
 * @(#)Configuration.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.configuration;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Properties;
import javax.swing.JOptionPane;
import org.fc.utils.Utilities;

/**
 * This class contains methods for manager the configuration
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public class Configuration {
    //Creando el archivo de propiedades
    private static Properties propiedades = null;

    /**
     * Metodo para poner un valor X
     *
     * @param propiedad
     * @param recurso
     */
    public static void setPropiedad(String propiedad, String recurso) {
        propiedades.setProperty(propiedad, recurso);
    }

    /**
     * Metodo para obtener un valor X de modo String
     *
     * @param recurso The key to search into properties file
     *
     * @return An instance of value as String
     */
    public static String getPropiedad(String recurso) {
        return propiedades.getProperty(recurso);
    }

    /**
     * Metodo para obtener un valor X de modo boolean
     *
     * @param recurso The key to search into properties file
     *
     * @return An instance of value as boolean
     */
    public static boolean getPropiedadBoolean(String recurso) {
        return Boolean.valueOf(propiedades.getProperty(recurso));
    }

    /**
     * Metodo para obtener un valor X de modo int
     *
     * @param recurso The key to search into properties file
     *
     * @return An instance of value as int
     */
    public static int getPropiedadInt(String recurso) {
        return Integer.valueOf(propiedades.getProperty(recurso));
    }

    //Metodo para guardar en un archivo config.properties la configuracion
    public static void guardar() {
        try {
            FileOutputStream guardarConfig = new FileOutputStream(Utilities.getOptionsFilePath());
            propiedades.store(guardarConfig, "Archivo de configuración de FCalculator\nSi se borra, se cargarán los valores predeterminados");
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(null, "Error al guardar la configuración", "ERROR", 0);
        }
    }

    //Metodo para poner los valores por default de las opciones
    public static void setDefaults() {
        FileConfiguration fileC = new FileConfiguration();

        fileC.readResource(CheckConfiguration.getResources());

        propiedades.putAll(fileC.getEntries());

        guardar();
    }

    public static void setOpc(HashMap<String, String> mapa) {
        propiedades.putAll(mapa);
        setVersion();
        guardar();
    }

    private static void setVersion() {
        setPropiedad("version", String.valueOf(CheckConfiguration.version));
        setPropiedad("major", String.valueOf(CheckConfiguration.major));
        setPropiedad("minor", String.valueOf(CheckConfiguration.minor));
    }

    //Cargando el archivo de propiedades para poder leer las configuraciones
    static {
        try {
            propiedades = Utilities.cargarPropiedades(Utilities.getOptionsFilePath());
        } catch(Exception io) {
            propiedades = new Properties();
            setDefaults();
        }
    }

    public static Properties getPropiedades() {
        return propiedades;
    }

    public Configuration() {
    }
}
