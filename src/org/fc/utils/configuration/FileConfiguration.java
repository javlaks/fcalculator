/*
 * @(#)FileConfiguration.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.configuration;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * This class contains methods for manager the configuration
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public class FileConfiguration {
    private HashMap<String, String> entriesMap;
    private Enumeration<Object> elementsAsObject, keysAsObject;
    private Enumeration<String> keysAsString;

    public FileConfiguration() {
        entriesMap = new HashMap<>();
    }

    public void readProperties(Properties properties) {
        try {
            elementsAsObject = properties.elements();
            keysAsObject = properties.keys();

            while(elementsAsObject.hasMoreElements() && keysAsObject.hasMoreElements()) {
                addEntry(keysAsObject.nextElement().toString(), elementsAsObject.nextElement().toString());
            }
        } catch(Exception e) {
            System.err.println(e);
        }
    }

    public void readResource(ResourceBundle resource) {
        try {
            keysAsString = resource.getKeys();

            while(keysAsString.hasMoreElements()) {
                String val = keysAsString.nextElement().toString();

                addEntry(val, resource.getString(val));
            }
        } catch(Exception e) {
            System.err.println(e);
        }
    }

    private void addEntry(String valor1, String valor2) {
        getEntries().put(valor1, valor2);
    }

    public HashMap<String, String> getEntries() {
        return entriesMap;
    }
}
