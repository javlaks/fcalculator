/*
 * @(#)FilterFiles.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 * This class contains a filter for a text (.txt) files.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 2.1
 * @version 2.1
 */
public class FilterFiles extends FileFilter {
    //metodo publico con valor de retorno booleano para determinar si acepta o no un tipo de fichero tiene un parametro del tipo File
    @Override
    public boolean accept(File f) {
        //Revisamos si el parametro f es un directorio
        if(f.isDirectory()) {
            return true;
        }

        String extension = Archivos.getExtension(f);
        if(extension != null) {
            if(extension.equals(Archivos.txt)) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    //metodo para regresar un string con las descripcion del filtro
    @Override
    public String getDescription() {
        return "Archivos de Texto (*.txt)";
    }
}

class Archivos {
    protected final static String txt = "txt";

    /*
     * Obtenemos la extension del archivo
     */
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if(i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }

    private Archivos() {
    }
}
