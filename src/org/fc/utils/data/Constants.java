/*
 * @(#)Constants.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.data;

/**
 * This class contains constants as a newline and double newline.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 2.0
 * @version 2.0
 */
public class Constants {
    private static final char saltoChar = 10;
    private static final char comillas = 34;
    public static final String salto = String.valueOf(saltoChar);
    public static final String salto2 = String.valueOf(saltoChar) + String.valueOf(saltoChar);
    public static final String com = String.valueOf(comillas);

    private Constants() {
    }
}
