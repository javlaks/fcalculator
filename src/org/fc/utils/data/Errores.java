/*
 * @(#)Errores.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.data;

/**
 * @author Victor Pineda
 * @see http://vscorpionblack.blogspot.com
 * @version 2.5
 */
/**
 * This class contains methods ans constants to show in error case.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 2.0
 * @version 3.0
 */
public class Errores {
    private static final String errEntero = "Se necesita un valor numérico entero.";
    private static final String errDecimal = "Se necesita un valor numérico.";
    private static final String errMayor = "No puede ser mayor o igual el inicio";
    private static final String errDiferencia = "Debe de haber 2 números almenos de diferencia";
    private static final String errSistemaNumerico = "El número ingresado no puede ser convertido, revísalo!";

    public static String errorEntero() {
        return errEntero;
    }

    public static String errorDecimal() {
        return errDecimal;
    }

    public static String errorMayor() {
        return errMayor;
    }

    public static String errorIgual() {
        return errDiferencia;
    }

    public static String errorSistemaNumerico() {
        return errSistemaNumerico;
    }

    private Errores() {
    }
}
