/*
 * @(#)VersionCheck.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.net;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.util.Properties;
import org.fc.utils.Utilities;
import org.fc.utils.configuration.CheckConfiguration;

/**
 * This class contains methods to check the new FCalculator version
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @author http://sourceforge.net/projects/jconvert
 * @since 2.2
 * @version 2.2
 */
public class VersionCheck {
    private int version;
    private int major;
    private int minor;
    private boolean error;

    public boolean existeNuevaVersion() {
        version = CheckConfiguration.version;
        major = CheckConfiguration.major;
        minor = CheckConfiguration.minor;
        setError(false);

        Properties propiedadesTemporal = null;
        StringBuffer bufferTemporal = null;

        try {
            URLConnection conexionLocalURL = Http.getPage(Utilities.VERSION_URL);
            bufferTemporal = Http.receivePage(conexionLocalURL, "UTF-8");
            propiedadesTemporal = new Properties();
            propiedadesTemporal.load(new ByteArrayInputStream(bufferTemporal.toString().getBytes("UTF-8")));
        } catch(IOException ioe) {
            setError(true);
            return false;
        }

        Boolean comprobador = comparar(propiedadesTemporal.getProperty("version"), version);
        if(comprobador != null) {
            return comprobador.booleanValue();
        }
        comprobador = comparar(propiedadesTemporal.getProperty("major"), major);
        if(comprobador != null) {
            return comprobador.booleanValue();
        }
        comprobador = comparar(propiedadesTemporal.getProperty("minor"), minor);
        if(comprobador != null) {
            return comprobador.booleanValue();
        }
        return false;

    }

    private Boolean comparar(String paramString1, int paramString2) {
        int i = Integer.valueOf(paramString1);
        int j = paramString2;
        if(i > j) {
            return Boolean.TRUE;
        }
        if(i < j) {
            return Boolean.FALSE;
        }

        return null;
    }

    public static int checkNuevaVersion() {

        Hilo hilo = new Hilo();

        return hilo.checkNuevaVersion();
    }

    /**
     * @return the error
     */
    public boolean isError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(boolean error) {
        this.error = error;
    }

    static class Hilo extends Thread {
        public int checkNuevaVersion() {
            VersionCheck vc = new VersionCheck();

            if(vc.existeNuevaVersion()) {
                return 1;
            } else {
                if(vc.isError() == true) {
                    return 2;
                } else {
                    return 3;
                }
            }
        }
    }
}
