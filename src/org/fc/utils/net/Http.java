/*
 * @(#)Http.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.net;

import java.awt.Desktop;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import org.fc.ex.ErrorData;
import org.jdesktop.swingx.JXErrorPane;

/**
 * This class contains methods for use the net.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @author http://sourceforge.net/projects/jconvert
 * @since 2.2
 * @version 4.1
 */
public class Http {
    public static StringBuffer receivePage(URLConnection paramURLConnection, String paramString) throws IOException {
        StringBuffer localStringBuffer;
        try (InputStreamReader localInputStreamReader = new InputStreamReader(paramURLConnection.getInputStream(), paramString)) {
            int i = 16384;
            char[] arrayOfChar = new char[i];
            int j = 0;
            localStringBuffer = new StringBuffer();
            int k = localInputStreamReader.read(arrayOfChar, 0, i);
            while(k != -1) {
                for(int l = 0; l < k; ++l) {
                    localStringBuffer.append(arrayOfChar[l]);
                }
                arrayOfChar = new char[i];
                k = localInputStreamReader.read(arrayOfChar, 0, i);
            }
        }

        return localStringBuffer;
    }

    public static URLConnection getPage(String paramString) {
        return getPage(paramString, null, null, true);
    }

    public static URLConnection getPage(String paramString1, String paramString2, String paramString3, boolean paramBoolean) {
        HttpURLConnection localHttpURLConnection;
        try {
            URL localURL = new URL(paramString1);
            URLConnection localURLConnection = localURL.openConnection();
            if(!(localURLConnection instanceof HttpURLConnection)) {
                return localURLConnection;
            }
            localHttpURLConnection = (HttpURLConnection) localURLConnection;
            localHttpURLConnection.setInstanceFollowRedirects(paramBoolean);

            localHttpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6");

            if(paramString3 != null) {
                localHttpURLConnection.setRequestProperty("Referer", paramString3);
            }
            if(paramString2 != null) {
                localHttpURLConnection.setRequestProperty("Cookie", paramString2);
            }
        } catch(Exception localException) {
            localException.printStackTrace();
            localHttpURLConnection = null;
        }
        return localHttpURLConnection;
    }
    private static Desktop desktop;

    public static void openURL(String link) {
        if(Desktop.isDesktopSupported()) {
            desktop = Desktop.getDesktop();
        }

        URI uri = null;

        if(desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                if(uri == null) {
                    uri = new URI(link);
                }
                desktop.browse(uri);
            } catch(URISyntaxException | IOException ex) {
                JXErrorPane.showDialog(null, new ErrorData(ex).getErrorInfo());
            }
        }
    }

    private Http() {
    }
}
