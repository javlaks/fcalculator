/*
 * @(#)ByteBuffer.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.net;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.CRC32;

/**
 * This class contains methods for read files.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @author http://sourceforge.net/projects/jconvert
 * @since 2.2
 * @version 2.2
 */
public class ByteBuffer {
    private byte[] data;
    private int length;
    private CRC32 crcCalc = new CRC32();
    private int crc;

    public ByteBuffer(byte[] paramArrayOfByte, int paramInt) {
        this.data = paramArrayOfByte;
        this.length = paramInt;

        this.crcCalc.reset();
        this.crcCalc.update(this.data);
        this.crc = (int) this.crcCalc.getValue();
    }

    public int getLength() {
        return this.length;
    }

    public byte[] getData() {
        return this.data;
    }

    public int getCRC() {
        return this.crc;
    }

    public void save(String paramString) {
        File localFile = new File(paramString);
        Object localObject;
        if(localFile.exists()) {
            localObject = new File(paramString + "~");
            if(((File) localObject).exists()) {
                ((File) localObject).delete();
            }
            localFile.renameTo((File) localObject);
        }
        try {
            localObject = new FileOutputStream(localFile);
            ((FileOutputStream) localObject).write(this.data, 0, this.length);
        } catch(IOException localIOException) {
            localIOException.printStackTrace();
        }
    }
}
