/*
 * @(#)OptionsUI.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.gui;

import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.DefaultComboBoxModel;
import org.fc.utils.configuration.CheckOptions;
import org.fc.utils.configuration.Configuration;
import org.fc.utils.look.LooksAndFeels;

/**
 * This class contains elements to create a GUI and manager app properties
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 2.0
 * @version 5.0
 */
public class OptionsUI extends FCDialog {
    private static final long serialVersionUID = 7236032136987058212L;
    private boolean previsualizar = false;
    private LooksAndFeels looks;
    private CheckOptions checkop = new CheckOptions();

    public OptionsUI() {
        System.gc();
        looks = new LooksAndFeels();
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupo1 = new javax.swing.ButtonGroup();
        panelPrincipal = new javax.swing.JPanel();
        panelOpciones = new javax.swing.JTabbedPane();
        lookPanel = new javax.swing.JPanel();
        looksLabel = new javax.swing.JLabel();
        looksList = new javax.swing.JComboBox();
        panelAplicaciones = new javax.swing.JPanel();
        casillaAplicacion = new javax.swing.JCheckBox();
        panelInicio = new javax.swing.JPanel();
        mensaje1 = new javax.swing.JLabel();
        casillaBienvenida = new javax.swing.JCheckBox();
        mensaje4 = new javax.swing.JLabel();
        casillaUpdate = new javax.swing.JCheckBox();
        mensaje5 = new javax.swing.JLabel();
        casillaMinimizado = new javax.swing.JCheckBox();
        panelSonidos = new javax.swing.JPanel();
        casillaSonidos = new javax.swing.JCheckBox();
        comboSonidos = new javax.swing.JComboBox();
        casillaPrevisualizar = new javax.swing.JCheckBox();
        panelSalida = new javax.swing.JPanel();
        mensaje3 = new javax.swing.JLabel();
        casillaMCerrar = new javax.swing.JCheckBox();
        mensaje2 = new javax.swing.JLabel();
        minimizeBoton = new javax.swing.JRadioButton();
        exitBoton = new javax.swing.JRadioButton();
        cerrarBoton = new javax.swing.JButton();
        defaultsBoton = new javax.swing.JButton();

        setTitle("Opciones");
        setModal(true);
        setResizable(false);

        panelPrincipal.setName("panelPrincipal"); // NOI18N

        panelOpciones.setName("panelOpciones"); // NOI18N

        lookPanel.setName("lookPanel"); // NOI18N
        lookPanel.setOpaque(false);

        looksLabel.setText("NOTA: Se requiere reiniciar FCalculator");
        looksLabel.setName("looksLabel"); // NOI18N

        looksList.setModel(setLooks());
        looksList.setSelectedItem(Configuration.getPropiedad("lookandfeel"));
        looksList.setName("looksList"); // NOI18N
        looksList.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                looksListItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout lookPanelLayout = new javax.swing.GroupLayout(lookPanel);
        lookPanel.setLayout(lookPanelLayout);
        lookPanelLayout.setHorizontalGroup(
            lookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lookPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(lookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(looksLabel)
                    .addComponent(looksList, 0, 398, Short.MAX_VALUE))
                .addContainerGap())
        );
        lookPanelLayout.setVerticalGroup(
            lookPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lookPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(looksLabel)
                .addGap(18, 18, 18)
                .addComponent(looksList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(167, Short.MAX_VALUE))
        );

        panelOpciones.addTab("Apariencia", lookPanel);

        panelAplicaciones.setName("panelAplicaciones"); // NOI18N

        casillaAplicacion.setSelected(Configuration.getPropiedadBoolean("en_panel"));
        casillaAplicacion.setText("Mostrar en aplicación principal");
        casillaAplicacion.setName("casillaAplicacion"); // NOI18N
        casillaAplicacion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                casillaAplicacionItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout panelAplicacionesLayout = new javax.swing.GroupLayout(panelAplicaciones);
        panelAplicaciones.setLayout(panelAplicacionesLayout);
        panelAplicacionesLayout.setHorizontalGroup(
            panelAplicacionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAplicacionesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(casillaAplicacion)
                .addContainerGap(243, Short.MAX_VALUE))
        );
        panelAplicacionesLayout.setVerticalGroup(
            panelAplicacionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAplicacionesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(casillaAplicacion)
                .addContainerGap(202, Short.MAX_VALUE))
        );

        panelOpciones.addTab("Aplicaciones", panelAplicaciones);

        panelInicio.setName("panelInicio"); // NOI18N
        panelInicio.setOpaque(false);

        mensaje1.setText("Mensaje inicial");
        mensaje1.setName("mensaje1"); // NOI18N

        casillaBienvenida.setMnemonic('b');
        casillaBienvenida.setSelected(!Configuration.getPropiedadBoolean("casillaBienvenida"));
        casillaBienvenida.setText("Desactivar mensaje inicial en Bandeja");
        casillaBienvenida.setName("casillaBienvenida"); // NOI18N
        casillaBienvenida.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                casillaBienvenidaItemStateChanged(evt);
            }
        });

        mensaje4.setText("Actualizaciones");
        mensaje4.setName("mensaje4"); // NOI18N

        casillaUpdate.setMnemonic('r');
        casillaUpdate.setSelected(Configuration.getPropiedadBoolean("update"));
        casillaUpdate.setText("Revisar nueva versión al iniciar");
        casillaUpdate.setName("casillaUpdate"); // NOI18N
        casillaUpdate.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                casillaUpdateItemStateChanged(evt);
            }
        });

        mensaje5.setText("Ventana principal");
        mensaje5.setName("mensaje5"); // NOI18N

        casillaMinimizado.setMnemonic('i');
        casillaMinimizado.setSelected(Configuration.getPropiedadBoolean("minimize"));
        casillaMinimizado.setText("Iniciar minimizado");
        casillaMinimizado.setName("casillaMinimizado"); // NOI18N
        casillaMinimizado.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                casillaMinimizadoItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout panelInicioLayout = new javax.swing.GroupLayout(panelInicio);
        panelInicio.setLayout(panelInicioLayout);
        panelInicioLayout.setHorizontalGroup(
            panelInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInicioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mensaje1)
                    .addComponent(mensaje4)
                    .addComponent(mensaje5)
                    .addGroup(panelInicioLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(panelInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(casillaBienvenida)
                            .addComponent(casillaUpdate)
                            .addComponent(casillaMinimizado))))
                .addContainerGap(191, Short.MAX_VALUE))
        );
        panelInicioLayout.setVerticalGroup(
            panelInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInicioLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mensaje1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(casillaBienvenida)
                .addGap(18, 18, 18)
                .addComponent(mensaje4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(casillaUpdate)
                .addGap(18, 18, 18)
                .addComponent(mensaje5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(casillaMinimizado)
                .addContainerGap(68, Short.MAX_VALUE))
        );

        panelOpciones.addTab("Inicio", panelInicio);

        panelSonidos.setName("panelSonidos"); // NOI18N
        panelSonidos.setOpaque(false);

        casillaSonidos.setMnemonic('e');
        casillaSonidos.setSelected(!Configuration.getPropiedadBoolean("casillaSonidos"));
        casillaSonidos.setText("Desactivar sonido al recibir un error");
        casillaSonidos.setName("casillaSonidos"); // NOI18N
        casillaSonidos.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                casillaSonidosItemStateChanged(evt);
            }
        });

        comboSonidos.setModel(getLista());
        comboSonidos.setSelectedItem(Configuration.getPropiedad("sonido"));
        comboSonidos.setEnabled(Configuration.getPropiedadBoolean("casillaSonidos"));
        comboSonidos.setName("comboSonidos"); // NOI18N
        comboSonidos.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboSonidosItemStateChanged(evt);
            }
        });

        casillaPrevisualizar.setMnemonic('p');
        casillaPrevisualizar.setText("Previsualizar");
        casillaPrevisualizar.setName("casillaPrevisualizar"); // NOI18N
        casillaPrevisualizar.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                casillaPrevisualizarItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout panelSonidosLayout = new javax.swing.GroupLayout(panelSonidos);
        panelSonidos.setLayout(panelSonidosLayout);
        panelSonidosLayout.setHorizontalGroup(
            panelSonidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSonidosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelSonidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboSonidos, 0, 398, Short.MAX_VALUE)
                    .addComponent(casillaPrevisualizar)
                    .addComponent(casillaSonidos))
                .addContainerGap())
        );
        panelSonidosLayout.setVerticalGroup(
            panelSonidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSonidosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(casillaSonidos)
                .addGap(18, 18, 18)
                .addComponent(casillaPrevisualizar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboSonidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(137, Short.MAX_VALUE))
        );

        panelOpciones.addTab("Sonidos", panelSonidos);

        panelSalida.setName("panelSalida"); // NOI18N
        panelSalida.setOpaque(false);

        mensaje3.setText("Mensaje salida");
        mensaje3.setName("mensaje3"); // NOI18N

        casillaMCerrar.setMnemonic('d');
        casillaMCerrar.setSelected(!Configuration.getPropiedadBoolean("mensajeCerrar"));
        casillaMCerrar.setText("Desactivar mensaje al salir");
        casillaMCerrar.setName("casillaMCerrar"); // NOI18N
        casillaMCerrar.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                casillaMCerrarItemStateChanged(evt);
            }
        });

        mensaje2.setText("Al presionar \"x\"");
        mensaje2.setName("mensaje2"); // NOI18N

        grupo1.add(minimizeBoton);
        minimizeBoton.setMnemonic('m');
        minimizeBoton.setSelected(Configuration.getPropiedadInt("cerrar") == 2 ? true : false);
        minimizeBoton.setText("Minimizar a la bandeja del sistema");
        minimizeBoton.setName("minimizeBoton"); // NOI18N
        minimizeBoton.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                minimizeBotonItemStateChanged(evt);
            }
        });

        grupo1.add(exitBoton);
        exitBoton.setMnemonic('s');
        exitBoton.setSelected(Configuration.getPropiedadInt("cerrar") == 3 ? true : false);
        exitBoton.setText("Salir de la aplicacion");
        exitBoton.setName("exitBoton"); // NOI18N
        exitBoton.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                exitBotonItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout panelSalidaLayout = new javax.swing.GroupLayout(panelSalida);
        panelSalida.setLayout(panelSalidaLayout);
        panelSalidaLayout.setHorizontalGroup(
            panelSalidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSalidaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelSalidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mensaje3)
                    .addComponent(mensaje2)
                    .addGroup(panelSalidaLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(panelSalidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(casillaMCerrar)
                            .addComponent(exitBoton)
                            .addComponent(minimizeBoton))))
                .addContainerGap(209, Short.MAX_VALUE))
        );
        panelSalidaLayout.setVerticalGroup(
            panelSalidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSalidaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mensaje3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(casillaMCerrar)
                .addGap(18, 18, 18)
                .addComponent(mensaje2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(minimizeBoton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(exitBoton)
                .addContainerGap(102, Short.MAX_VALUE))
        );

        panelOpciones.addTab("Salida", panelSalida);

        cerrarBoton.setMnemonic('c');
        cerrarBoton.setText("Cerrar");
        cerrarBoton.setToolTipText("Guarda los cambios efectuados."); // NOI18N
        cerrarBoton.setName("cerrarBoton"); // NOI18N
        cerrarBoton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarBotonActionPerformed(evt);
            }
        });

        defaultsBoton.setMnemonic('f');
        defaultsBoton.setText("Defaults");
        defaultsBoton.setToolTipText("Reinicia a sus valores por default todas las opciones."); // NOI18N
        defaultsBoton.setName("defaultsBoton"); // NOI18N
        defaultsBoton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                defaultsBotonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrincipalLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(defaultsBoton)
                .addGap(18, 18, 18)
                .addComponent(cerrarBoton)
                .addContainerGap())
            .addComponent(panelOpciones, javax.swing.GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE)
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrincipalLayout.createSequentialGroup()
                .addComponent(panelOpciones)
                .addGap(18, 18, 18)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cerrarBoton)
                    .addComponent(defaultsBoton))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void minimizeBotonItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_minimizeBotonItemStateChanged
        Configuration.setPropiedad("cerrar", "2");
        guardar();
    }//GEN-LAST:event_minimizeBotonItemStateChanged

    private void exitBotonItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_exitBotonItemStateChanged
        Configuration.setPropiedad("cerrar", "3");
        guardar();
    }//GEN-LAST:event_exitBotonItemStateChanged

    private void cerrarBotonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarBotonActionPerformed
        dispose();
    }//GEN-LAST:event_cerrarBotonActionPerformed

    private void casillaBienvenidaItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_casillaBienvenidaItemStateChanged
    {//GEN-HEADEREND:event_casillaBienvenidaItemStateChanged
        if(casillaBienvenida.isSelected()) {
            Configuration.setPropiedad("casillaBienvenida", "false");
        } else {
            Configuration.setPropiedad("casillaBienvenida", "true");
        }
        guardar();
    }//GEN-LAST:event_casillaBienvenidaItemStateChanged

    private void casillaSonidosItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_casillaSonidosItemStateChanged
    {//GEN-HEADEREND:event_casillaSonidosItemStateChanged
        if(casillaSonidos.isSelected()) {
            Configuration.setPropiedad("casillaSonidos", "false");
            casillaPrevisualizar.setEnabled(false);
            comboSonidos.setEnabled(false);
        } else {
            Configuration.setPropiedad("casillaSonidos", "true");
            casillaPrevisualizar.setEnabled(true);
            comboSonidos.setEnabled(true);
        }
        guardar();
    }//GEN-LAST:event_casillaSonidosItemStateChanged

    private void casillaMCerrarItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_casillaMCerrarItemStateChanged
    {//GEN-HEADEREND:event_casillaMCerrarItemStateChanged
        if(casillaMCerrar.isSelected()) {
            Configuration.setPropiedad("mensajeCerrar", "false");
        } else {
            Configuration.setPropiedad("mensajeCerrar", "true");
        }
        guardar();
    }//GEN-LAST:event_casillaMCerrarItemStateChanged

    private void comboSonidosItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_comboSonidosItemStateChanged
    {//GEN-HEADEREND:event_comboSonidosItemStateChanged
        if(evt.getStateChange() == ItemEvent.SELECTED) {
            Configuration.setPropiedad("sonido", String.valueOf(evt.getItem()));

            if(previsualizar) {
                checkop.playSound(String.valueOf(evt.getItem()));
            }

            guardar();
        }
    }//GEN-LAST:event_comboSonidosItemStateChanged

    private void casillaUpdateItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_casillaUpdateItemStateChanged
    {//GEN-HEADEREND:event_casillaUpdateItemStateChanged
        if(casillaUpdate.isSelected()) {
            Configuration.setPropiedad("update", "true");
        } else {
            Configuration.setPropiedad("update", "false");
        }
        guardar();
    }//GEN-LAST:event_casillaUpdateItemStateChanged

    private void casillaMinimizadoItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_casillaMinimizadoItemStateChanged
    {//GEN-HEADEREND:event_casillaMinimizadoItemStateChanged
        if(casillaMinimizado.isSelected()) {
            Configuration.setPropiedad("minimize", "true");
        } else {
            Configuration.setPropiedad("minimize", "false");
        }
        guardar();
    }//GEN-LAST:event_casillaMinimizadoItemStateChanged

    private void casillaPrevisualizarItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_casillaPrevisualizarItemStateChanged
    {//GEN-HEADEREND:event_casillaPrevisualizarItemStateChanged
        if(casillaPrevisualizar.isSelected()) {
            previsualizar = true;
        } else {
            previsualizar = false;
        }
    }//GEN-LAST:event_casillaPrevisualizarItemStateChanged

    private void defaultsBotonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_defaultsBotonActionPerformed
    {//GEN-HEADEREND:event_defaultsBotonActionPerformed
        Configuration.setDefaults();
    }//GEN-LAST:event_defaultsBotonActionPerformed

    private void looksListItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_looksListItemStateChanged
    {//GEN-HEADEREND:event_looksListItemStateChanged
        if(evt.getStateChange() == ItemEvent.SELECTED) {
            Configuration.setPropiedad("lookandfeel", looksList.getSelectedItem().toString());
            guardar();
        }
    }//GEN-LAST:event_looksListItemStateChanged

    private void casillaAplicacionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_casillaAplicacionItemStateChanged
        if(casillaAplicacion.isSelected()) {
            Configuration.setPropiedad("en_panel", "true");
        } else {
            Configuration.setPropiedad("en_panel", "false");
        }
        guardar();
    }//GEN-LAST:event_casillaAplicacionItemStateChanged
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox casillaAplicacion;
    private javax.swing.JCheckBox casillaBienvenida;
    private javax.swing.JCheckBox casillaMCerrar;
    private javax.swing.JCheckBox casillaMinimizado;
    private javax.swing.JCheckBox casillaPrevisualizar;
    private javax.swing.JCheckBox casillaSonidos;
    private javax.swing.JCheckBox casillaUpdate;
    private javax.swing.JButton cerrarBoton;
    private javax.swing.JComboBox comboSonidos;
    private javax.swing.JButton defaultsBoton;
    private javax.swing.JRadioButton exitBoton;
    private javax.swing.ButtonGroup grupo1;
    private javax.swing.JPanel lookPanel;
    private javax.swing.JLabel looksLabel;
    private javax.swing.JComboBox looksList;
    private javax.swing.JLabel mensaje1;
    private javax.swing.JLabel mensaje2;
    private javax.swing.JLabel mensaje3;
    private javax.swing.JLabel mensaje4;
    private javax.swing.JLabel mensaje5;
    private javax.swing.JRadioButton minimizeBoton;
    private javax.swing.JPanel panelAplicaciones;
    private javax.swing.JPanel panelInicio;
    private javax.swing.JTabbedPane panelOpciones;
    private javax.swing.JPanel panelPrincipal;
    private javax.swing.JPanel panelSalida;
    private javax.swing.JPanel panelSonidos;
    // End of variables declaration//GEN-END:variables

    private void guardar() {
        Configuration.guardar();
    }

    private javax.swing.DefaultComboBoxModel getLista() {
        javax.swing.DefaultComboBoxModel dcbm;
        String listaSonidos[] = checkop.getLista();
        dcbm = new javax.swing.DefaultComboBoxModel(listaSonidos);
        return dcbm;
    }

    private DefaultComboBoxModel setLooks() {
        ArrayList<String> list = looks.getLookAndFeelInstalled();
        Collections.sort(list);
        return new DefaultComboBoxModel(list.toArray());
    }
}
