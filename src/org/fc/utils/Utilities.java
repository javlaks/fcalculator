/*
 * @(#)Utilities.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils;

import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 * This class contains methods ans constants necesary for FCalculator.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public class Utilities {
    public static final String BLOG_URL = "http://vscorpionblack.blogspot.com";
    public static final String VERSION_URL = "http://fcalculator.sourceforge.net/Version.properties";
    public static final String FCALCULATOR_URL = "http://fcalculator.sourceforge.net";
    public static final String FCALCULATOR_DOWNLOAD_URL = "http://sourceforge.net/projects/fcalculator/files";
    public static final String FCALCULATOR_PROJECT_URL = "http://sourceforge.net/projects/fcalculator";
    public static final String FCALCULATOR_LICENSE_URL = "http://fcalculator.sourceforge.net/licencia.html";
    public static final String FCALCULATOR_CHANGELOG_URL = "http://fcalculator.sourceforge.net/changelog.html";
    public static final String CONFIGURATION_FILE = "fc_config.properties";
    public static final String FILENAME = "FCalculator.jar";

    public Utilities() {
    }

    public static String getPath() {
        String path;
        String tempPath;

        try {
            tempPath = String.valueOf(new File(Utilities.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()));

            try {
                path = tempPath.substring(0, tempPath.lastIndexOf(FILENAME));
            } catch(StringIndexOutOfBoundsException io) {
                path = tempPath.substring(0, tempPath.lastIndexOf("build"));
            }

            return path;
        } catch(URISyntaxException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static String getOptionsFilePath() {
        return getPath() + CONFIGURATION_FILE;
    }

    public static InputStream getResourceAsStream(String recurso) throws Exception {
        Object objetoLocal = null;

        ClassLoader localClassLoader = Utilities.class.getClassLoader();
        objetoLocal = localClassLoader.getResourceAsStream(recurso);

        if(objetoLocal != null) {
            return (InputStream) objetoLocal;
        }

        localClassLoader = Thread.currentThread().getContextClassLoader();
        objetoLocal = localClassLoader.getResourceAsStream(recurso);

        if(objetoLocal != null) {
            return (InputStream) objetoLocal;
        }

        try {
            objetoLocal = new FileInputStream(recurso);
        } catch(Exception localException) {
            throw localException;
        }
        return ((InputStream) objetoLocal);
    }

    public static Properties cargarPropiedades(String recurso) throws IOException, Exception {
        return cargarPropiedades(getResourceAsStream(recurso));
    }

    public static Properties cargarPropiedades(InputStream recursoEntrada) throws IOException {
        if(recursoEntrada == null) {
            throw new IOException("No se pudo cargar el archivo de propiedades desde el inputstream - esto es nulo");
        }

        Properties propiedades = null;

        synchronized(Utilities.class) {
            try {
                propiedades = new Properties();
                propiedades.load(recursoEntrada);
                recursoEntrada.close();
            } catch(IOException localIOException) {
                throw localIOException;
            }
        }

        return propiedades;
    }

    public static Image getImage(String file) {
        return getImageIcon(file).getImage();
    }

    public static ImageIcon getImageIcon(String file) {
        java.net.URL imgURL = Utilities.class.getResource(file);

        if(imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            return null;
        }
    }
}
