/*
 * @(#)Enteros.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.utils.numbers;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * This class contains methods for make a filter integer number into JTextField
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 3.0
 * @version 3.0
 */
public class Enteros extends PlainDocument {
    private static final long serialVersionUID = 9083948246399748089L;

    /**
     * Método al que llama el editor cada vez que se intenta insertar caracteres. Sólo debemos
     * verificar arg1, que es la cadena que se quiere insertar en el JTextField
     *
     * @param arg0
     * @param arg1
     * @param arg2
     */
    @Override
    public void insertString(int arg0, String arg1, AttributeSet arg2) throws BadLocationException {
        for(int i = 0; i < arg1.length(); i++) {
            if(!Character.isDigit(arg1.charAt(i)) && (Character.valueOf(arg1.charAt(i)) != '-')) {
                return;
            }
        }

        super.insertString(arg0, arg1, arg2);
    }
}
