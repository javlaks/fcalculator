/*
 * @(#)Conversor.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.apps.convert;

import java.math.BigDecimal;
import java.util.ResourceBundle;
import org.fc.utils.data.Constants;

/**
 * This class contains methods and constants requeried for the conversion.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 3.1
 * @version 3.1
 */
class Conversor {
    /**
     *
     */
    private static String resultado;
    private static String salto = Constants.salto;
    private static ResourceBundle recursoConversor = null;

    /**
     * Este metodo sirve para poner los resultados en una variable y posteriormente mostrarla
     *
     * @param temporal Es el valor que recibe y lo almacena
     */
    protected static void setData(String temporal) {
        setResultado(temporal);
    }

    /**
     * Este metodo nos devolvera el valor del resultado almacenado
     *
     * @return El resultado como una variable tipo String
     */
    public static String getData() {
        return getResultado();
    }

    /**
     * @return the resultado
     */
    public static String getResultado() {
        return resultado;
    }

    /**
     * @param aResultado the resultado to set
     */
    public static void setResultado(String aResultado) {
        resultado = aResultado;
    }

    /**
     * @return the salto
     */
    public static String getSalto() {
        return salto;
    }

    /**
     * @param aSalto the salto to set
     */
    public static void setSalto(String aSalto) {
        salto = aSalto;
    }

    /**
     * Constructor privado de la clase
     */
    protected Conversor() {
    }

    protected static BigDecimal getUnit(String categoria, String de, String a) {
        String valor = categoria + "," + de + "," + a;

        return new BigDecimal(recursoConversor.getString(valor));
    }

    /**
     * Este metodo permite la conversion para mas exactitud utilizando la clase BigDecimal.
     *
     * @param valor Recibe el valor el cual se quiere obtener la mayor exactitud posible
     * @param temp
     *
     * @return El valor como un String
     */
    protected static String convertir(BigDecimal valor, BigDecimal temp) {
        BigDecimal temporal;

        temporal = valor.multiply(temp);

        return String.valueOf(temporal);
    }

    static {
        recursoConversor = ResourceBundle.getBundle("org/fc/files/properties/conversor");
    }
}
