/*
 * @(#)TemperaturaMetodos.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.apps.convert;

/**
 * This class contains the methods to determinate the weather.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 1.0
 * @version 2.0
 */
class TemperaturaMetodos extends Conversor {
    private static final String c = "º grados celsius" + getSalto();
    private static final String f = "º grados fahrenheit" + getSalto();
    private static final String k = "º grados kelvin" + getSalto();
    private static final String r = "º grados rankine" + getSalto();
    private static final String re = "º grados réaumur";
    private static double cel, faren, kel, ran, rea;

    public static void celcius(double temp) {
        cel = temp;

        faren = (temp * 1.8) + 32;

        kel = temp + 273.15;

        ran = (temp + 273.15) * 9 / 5;

        rea = temp * 4 / 5;

        setDatos();
    }

    public static void fahrenheit(double temp) {
        faren = temp;

        cel = (temp - 32) / 1.8;

        kel = (temp + 459.67) / 1.8;

        ran = temp + 459.67;

        rea = (temp - 32) * 4 / 9;

        setDatos();
    }

    public static void kelvin(double temp) {
        kel = temp;

        cel = temp - 273.15;

        faren = (1.8 * temp) - 459.67;

        ran = temp * 9 / 5;

        rea = (temp - 273.15) * 4 / 5;

        setDatos();
    }

    public static void rankine(double temp) {
        ran = temp;

        cel = (temp - 491.67) * 5 / 9;

        faren = (temp - 459.67);

        kel = temp * 5 / 9;

        rea = (temp - 491.67) * 4 / 9;

        setDatos();
    }

    public static void reaumur(double temp) {
        rea = temp;

        cel = temp * 5 / 4;

        faren = (temp * 9 / 4) + 32;

        kel = (temp * 5 / 4) + 273.15;

        ran = (temp * 9 / 4) + 491.67;

        setDatos();
    }

    private static void setDatos() {
        setData("" + cel + c + faren + f + kel + k + ran + r + rea + re);
    }
}
