/*
 * @(#)VelocidadDatosMetodos.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.apps.convert;

/**
 * This class contains the method to determinate the velocity of data transmition.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
class VelocidadDatosMetodos extends Conversor {
    private static double Bs, KBs, MBs, GBs, TBs, bits;

    public static void bps(double valor) {
        bits = valor;

        Bs = bits / 8;
        KBs = Bs / 1000;
        MBs = KBs / 1000;
        GBs = MBs / 1000;
        TBs = GBs / 1000;

        setDatos();
    }

    public static void kbps(double valor) {
        bits = valor * Math.pow(10, 3);

        Bs = bits / 8;
        KBs = Bs / 1000;
        MBs = KBs / 1000;
        GBs = MBs / 1000;
        TBs = GBs / 1000;

        setDatos();
    }

    public static void mbps(double valor) {
        bits = valor * Math.pow(10, 6);

        Bs = bits / 8;
        KBs = Bs / 1000;
        MBs = KBs / 1000;
        GBs = MBs / 1000;
        TBs = GBs / 1000;

        setDatos();
    }

    public static void gbps(double valor) {
        bits = valor * Math.pow(10, 9);

        Bs = bits / 8;
        KBs = Bs / 1000;
        MBs = KBs / 1000;
        GBs = MBs / 1000;
        TBs = GBs / 1000;

        setDatos();
    }

    public static void tbps(double valor) {
        bits = valor * Math.pow(10, 12);

        Bs = bits / 8;
        KBs = Bs / 1000;
        MBs = KBs / 1000;
        GBs = MBs / 1000;
        TBs = GBs / 1000;

        setDatos();
    }

    private static void setDatos() {
        setData("B/s " + Bs + getSalto() + "KB/s " + KBs + getSalto() + "MB/s " + MBs + getSalto()
                + "GB/s " + GBs + getSalto() + "TB/s " + TBs + getSalto());
    }
}
