/*
 * @(#)ConversorMetodos.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.apps.convert;

import java.math.BigDecimal;

/**
 * This class contains the methods to calculate the conversion.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 3.1
 * @version 3.1
 */
class ConversorMetodos extends Conversor {
    public String convertir(String categoria, String de, String a, BigDecimal valor) {
        return convertir(valor, getUnit(categoria, de, a));
    }

    public String getWord(String word) {
        switch(word) {
            case "Años":
                return "Anios";
            case "Centímetros":
                return "Centimetros";
            case "Kilómetros":
                return "Kilometros";
            case "Días":
                return "Dias";
            case "Kilómetros/hora":
                return "KmH";
            case "Metros/segundo":
                return "MS";
            case "Millas/hora":
                return "MH";
            case "Pies/hora":
                return "PH";
            default:
                return word;
        }
    }
}
