/*
 * @(#)RandomNumberUI.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.apps.calc.ex;

import java.awt.event.KeyEvent;
import org.fc.algorithms.RandomNumber;
import org.fc.utils.WinLocation;
import org.fc.utils.configuration.Configuration;
import org.fc.utils.data.Errores;
import org.fc.utils.gui.FCDialog;
import org.fc.utils.numbers.Decimales;
import org.fc.utils.numbers.Enteros;

/**
 * This class contains the components to create de RandomNumber GUI.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 2.0
 * @version 4.0
 */
public class RandomNumberUI extends FCDialog {
    private static final long serialVersionUID = 2973773537208601311L;
    private String desicion = "Entero";

    public RandomNumberUI() {
        System.gc();
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoBotones = new javax.swing.ButtonGroup();
        panelPrincipal = new javax.swing.JPanel();
        generarBoton = new javax.swing.JButton();
        cerrarBoton = new javax.swing.JButton();
        campo1 = new javax.swing.JTextField();
        campo2 = new javax.swing.JTextField();
        campoResultado = new javax.swing.JTextField();
        yEtiqueta = new javax.swing.JLabel();
        resultadoEtiqueta = new javax.swing.JLabel();
        generarEtiqueta = new javax.swing.JLabel();
        enteroBoton = new javax.swing.JRadioButton();
        decimalBoton = new javax.swing.JRadioButton();
        ayudaEtiqueta = new javax.swing.JLabel();

        setTitle("Número Aleatorio");
        setModal(true);
        setResizable(false);

        panelPrincipal.setName("panelPrincipal"); // NOI18N

        generarBoton.setMnemonic('g');
        generarBoton.setText("Generar");
        generarBoton.setName("generarBoton"); // NOI18N
        generarBoton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generarBotonActionPerformed(evt);
            }
        });

        cerrarBoton.setMnemonic('A');
        cerrarBoton.setText("Aceptar");
        cerrarBoton.setName("cerrarBoton"); // NOI18N
        cerrarBoton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarBotonActionPerformed(evt);
            }
        });

        campo1.setDocument(Configuration.getPropiedadBoolean("random_entero") ? new Enteros() : new Decimales());
        campo1.setText(Configuration.getPropiedad("random_val1"));
        campo1.setName("campo1"); // NOI18N
        campo1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                campo1FocusGained(evt);
            }
        });
        campo1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                campo1KeyPressed(evt);
            }
        });

        campo2.setDocument(Configuration.getPropiedadBoolean("random_entero") ? new Enteros() : new Decimales());
        campo2.setText(Configuration.getPropiedad("random_val2"));
        campo2.setName("campo2"); // NOI18N
        campo2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                campo2FocusGained(evt);
            }
        });
        campo2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                campo2KeyPressed(evt);
            }
        });

        campoResultado.setEditable(false);
        campoResultado.setText("0");
        campoResultado.setName("campoResultado"); // NOI18N
        campoResultado.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                campoResultadoFocusGained(evt);
            }
        });

        yEtiqueta.setText("y");
        yEtiqueta.setName("yEtiqueta"); // NOI18N

        resultadoEtiqueta.setText("Resultado");
        resultadoEtiqueta.setName("resultadoEtiqueta"); // NOI18N

        generarEtiqueta.setText("Genera un número entre");
        generarEtiqueta.setName("generarEtiqueta"); // NOI18N

        grupoBotones.add(enteroBoton);
        enteroBoton.setSelected(Configuration.getPropiedadBoolean("random_entero"));
        enteroBoton.setText("Entero");
        enteroBoton.setName("enteroBoton"); // NOI18N
        enteroBoton.setOpaque(false);
        enteroBoton.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                enteroBotonItemStateChanged(evt);
            }
        });

        grupoBotones.add(decimalBoton);
        decimalBoton.setSelected(Configuration.getPropiedadBoolean("random_decimal"));
        decimalBoton.setText("Decimal");
        decimalBoton.setName("decimalBoton"); // NOI18N
        decimalBoton.setOpaque(false);
        decimalBoton.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                decimalBotonItemStateChanged(evt);
            }
        });

        ayudaEtiqueta.setFont(new java.awt.Font("Tahoma", 1, 11));
        ayudaEtiqueta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ayudaEtiqueta.setText("?");
        ayudaEtiqueta.setToolTipText("NOTA: No se incluyen los números ingresados."); // NOI18N
        ayudaEtiqueta.setName("ayudaEtiqueta"); // NOI18N

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addComponent(enteroBoton)
                        .addGap(18, 18, 18)
                        .addComponent(decimalBoton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 267, Short.MAX_VALUE)
                        .addComponent(ayudaEtiqueta, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addComponent(generarBoton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 282, Short.MAX_VALUE)
                        .addComponent(cerrarBoton))
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addComponent(resultadoEtiqueta)
                        .addGap(18, 18, 18)
                        .addComponent(campoResultado, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE))
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addComponent(generarEtiqueta)
                        .addGap(18, 18, 18)
                        .addComponent(campo1, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(yEtiqueta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(campo2, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(enteroBoton)
                    .addComponent(decimalBoton)
                    .addComponent(ayudaEtiqueta))
                .addGap(28, 28, 28)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(generarEtiqueta)
                    .addComponent(campo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(campo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(yEtiqueta))
                .addGap(36, 36, 36)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(resultadoEtiqueta)
                    .addComponent(campoResultado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(generarBoton)
                    .addComponent(cerrarBoton))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cerrarBotonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cerrarBotonActionPerformed
    {//GEN-HEADEREND:event_cerrarBotonActionPerformed
        Configuration.setPropiedad("random_val1", campo1.getText());
        Configuration.setPropiedad("random_val2", campo2.getText());
        guardar();
        dispose();
    }//GEN-LAST:event_cerrarBotonActionPerformed

    private void generarBotonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_generarBotonActionPerformed
    {//GEN-HEADEREND:event_generarBotonActionPerformed
        calcular();
    }//GEN-LAST:event_generarBotonActionPerformed

    private void enteroBotonItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_enteroBotonItemStateChanged
    {//GEN-HEADEREND:event_enteroBotonItemStateChanged
        if(enteroBoton.isSelected()) {
            Configuration.setPropiedad("random_entero", "true");

            String val1, val2;
            val1 = campo1.getText();
            val2 = campo2.getText();

            campo1.setDocument(new Enteros());
            campo2.setDocument(new Enteros());
            campo1.setText(String.valueOf(Math.round(Double.valueOf(val1))));
            campo2.setText(String.valueOf(Math.round(Double.valueOf(val2))));

            desicion = "Entero";
        } else {
            Configuration.setPropiedad("random_entero", "false");
        }
        guardar();
    }//GEN-LAST:event_enteroBotonItemStateChanged

    private void decimalBotonItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_decimalBotonItemStateChanged
    {//GEN-HEADEREND:event_decimalBotonItemStateChanged
        if(decimalBoton.isSelected()) {
            Configuration.setPropiedad("random_decimal", "true");

            String val1, val2;
            val1 = campo1.getText();
            val2 = campo2.getText();

            campo1.setDocument(new Decimales());
            campo2.setDocument(new Decimales());
            campo1.setText(val1);
            campo2.setText(val2);

            desicion = "Decimal";
        } else {
            Configuration.setPropiedad("random_decimal", "false");
        }
        guardar();
    }//GEN-LAST:event_decimalBotonItemStateChanged

    private void campo1FocusGained(java.awt.event.FocusEvent evt)//GEN-FIRST:event_campo1FocusGained
    {//GEN-HEADEREND:event_campo1FocusGained
        campo1.selectAll();
    }//GEN-LAST:event_campo1FocusGained

    private void campo1KeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_campo1KeyPressed
    {//GEN-HEADEREND:event_campo1KeyPressed
        int key = evt.getKeyCode();
        if(key == KeyEvent.VK_ENTER) {
            campo2.requestFocus();
        }
    }//GEN-LAST:event_campo1KeyPressed

    private void campo2KeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_campo2KeyPressed
    {//GEN-HEADEREND:event_campo2KeyPressed
        int key = evt.getKeyCode();
        if(key == KeyEvent.VK_ENTER) {
            calcular();
        }
    }//GEN-LAST:event_campo2KeyPressed

    private void campo2FocusGained(java.awt.event.FocusEvent evt)//GEN-FIRST:event_campo2FocusGained
    {//GEN-HEADEREND:event_campo2FocusGained
        campo2.selectAll();
    }//GEN-LAST:event_campo2FocusGained

    private void campoResultadoFocusGained(java.awt.event.FocusEvent evt)//GEN-FIRST:event_campoResultadoFocusGained
    {//GEN-HEADEREND:event_campoResultadoFocusGained
        campoResultado.selectAll();
    }//GEN-LAST:event_campoResultadoFocusGained
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ayudaEtiqueta;
    private javax.swing.JTextField campo1;
    private javax.swing.JTextField campo2;
    private javax.swing.JTextField campoResultado;
    private javax.swing.JButton cerrarBoton;
    private javax.swing.JRadioButton decimalBoton;
    private javax.swing.JRadioButton enteroBoton;
    private javax.swing.JButton generarBoton;
    private javax.swing.JLabel generarEtiqueta;
    private javax.swing.ButtonGroup grupoBotones;
    private javax.swing.JPanel panelPrincipal;
    private javax.swing.JLabel resultadoEtiqueta;
    private javax.swing.JLabel yEtiqueta;
    // End of variables declaration//GEN-END:variables

    @Override
    public void shows() {
        getSelectedOption();
        WinLocation.setLocation(this);

    }

    private void calcular() {
        try {
            switch(desicion) {
                case "Entero":
                    {
                        Long inicio = Long.valueOf(campo1.getText());
                        Long fin = Long.valueOf(campo2.getText());
                        if(inicio < 0) {
                            campo1.setText(String.valueOf(Math.abs(inicio)));
                            inicio = Long.valueOf(campo1.getText());
                        }
                        if(fin < 0) {
                            campo2.setText(String.valueOf(Math.abs(fin)));
                            fin = Long.valueOf(campo2.getText());
                        }
                        if(inicio >= fin) {
                            campoResultado.setText(Errores.errorMayor());

                            campo1.requestFocus();

                            return;
                        }
                        if(invalido(inicio, fin)) {
                            campoResultado.setText(Errores.errorIgual());

                            campo2.requestFocus();

                            return;
                        }
                        RandomNumber.entero(inicio, fin);
                        campoResultado.setText(RandomNumber.getRes());
                        break;
                    }
                case "Decimal":
                    {
                        Double inicio = Double.valueOf(campo1.getText());
                        Double fin = Double.valueOf(campo2.getText());
                        if(inicio < 0) {
                            campo1.setText(String.valueOf(Math.abs(inicio)));
                            inicio = Double.valueOf(campo1.getText());
                        }
                        if(fin < 0) {
                            campo2.setText(String.valueOf(Math.abs(fin)));
                            fin = Double.valueOf(campo2.getText());
                        }
                        if(inicio >= fin) {
                            campoResultado.setText(Errores.errorMayor());

                            campo1.requestFocus();

                            return;
                        }
                        RandomNumber.decimal(inicio, fin);
                        campoResultado.setText(RandomNumber.getRes());
                        break;
                    }
            }
        } catch(NumberFormatException nfe) {
            checkOptions();
            switch(desicion) {
                case "Entero":
                    campoResultado.setText(Errores.errorEntero());
                    break;
                case "Decimal":
                    campoResultado.setText(Errores.errorDecimal());
                    break;
            }
            campoResultado.setCaretPosition(1);
        }
    }

    private boolean invalido(long a, long b) {
        if((b - 2) >= a) {
            return false;
        } else {
            return true;
        }
    }

    public String getNumber() {
        return campoResultado.getText();
    }

    public void setNumber(String number) {
        campoResultado.setText(number);
    }

    private void guardar() {
        Configuration.guardar();
    }

    private void getSelectedOption() {
        if(enteroBoton.isSelected()) {
            desicion = "Entero";
        } else {
            desicion = "Decimal";
        }
    }
}
