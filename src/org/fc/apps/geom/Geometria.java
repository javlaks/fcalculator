/*
 * @(#)Geometria.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.apps.geom;

import org.fc.utils.data.Constants;

/**
 * This class contains the base for the geo.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public abstract class Geometria {
    private String resultado;
    private String salto2 = Constants.salto2;

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    /**
     * @return the salto2
     */
    public String getSalto2() {
        return salto2;
    }

    /**
     * @param salto2 the salto2 to set
     */
    public void setSalto2(String salto2) {
        this.salto2 = salto2;
    }
}
