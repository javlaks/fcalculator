/*
 * @(#)Circulo.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.apps.geom.planos;

/**
 * This class contains methods for circle.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public class Circulo extends Planos {
    final private double PI = Math.PI;
    private double radio;
    private double diametro;

    public double getDiametro() {
        return diametro;
    }

    public double getRadio() {
        return radio;
    }

    /**
     *
     * @param radio El radio del Circulo
     * @param diametro El diametro del Circulo
     */
    public Circulo(double radio, double diametro) {
        this.radio = radio;
        this.diametro = diametro;

        calcular();
    }

    private void calcular() {
        if(radio <= 0 && diametro <= 0) {
            setResultado("Tus datos están mal, revísalos.");
            return;
        }

        if(diametro <= 0 && radio > 0) {
            diametro = radio * 2;
        }

        if(radio <= 0 && diametro > 0) {
            radio = diametro / 2;
        }

        diametro = radio * 2;

        setPerimetro((2 * (PI) * (radio)));

        setArea((PI * (radio * radio)));

        setResultado(("Perímetro = " + getPerimetro() + getSalto2() + "Área = " + getArea()));
    }
}
