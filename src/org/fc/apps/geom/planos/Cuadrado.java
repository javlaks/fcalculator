/*
 * @(#)Cuadrado.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.apps.geom.planos;

/**
 * This class contains methods for square.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public class Cuadrado extends Planos {
    private double lado;
    private double diagonal;

    public double getDiagonal() {
        return diagonal;
    }

    public double getLado() {
        return lado;
    }

    /**
     *
     * @param lado
     * @param diagonal
     */
    public Cuadrado(double lado, double diagonal) {
        this.lado = lado;
        this.diagonal = diagonal;

        calcular();
    }

    private void calcular() {
        if(diagonal == 0 && lado == 0) {
            setResultado("Tus datos están mal, revísalos.");
            return;
        }

        if(diagonal != 0 && lado == 0) {
            lado = 0.7071 * diagonal;
        }

        if(lado != 0 && diagonal == 0) {
            diagonal = 1.414 * lado;
        }

        setPerimetro(lado * 4);

        setArea(lado * lado);

        setResultado(("Perímetro = " + getPerimetro() + getSalto2() + "Área = " + getArea()));
    }
}
