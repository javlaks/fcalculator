/*
 * @(#)TrianguloRectangulo.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.apps.geom.planos;

/**
 * This class contains methods for rectangle triangle.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.0
 */
public class TrianguloRectangulo extends Planos {
    private double base;
    private double altura;
    private double hipotenusa;

    public double getHipotenusa() {
        return hipotenusa;
    }

    public double getBase() {
        return base;
    }

    public double getAltura() {
        return altura;
    }

    /**
     *
     * @param ladoa
     * @param ladob
     * @param ladoc
     */
    public TrianguloRectangulo(double ladoa, double ladob, double ladoc) {
        this.base = ladoa;
        this.altura = ladob;
        this.hipotenusa = ladoc;

        calcular();
    }

    private void calcular() {
        if((hipotenusa == 0 && base == 0) || (hipotenusa == 0 && altura == 0) || (base == 0 && altura == 0)) {
            setResultado("Tus datos están mal, revísalos.");
            return;
        }

        if((base != 0 && altura != 0)) {
            hipotenusa = Math.sqrt(Math.pow(base, 2) + Math.pow(altura, 2));
        }

        if(hipotenusa <= base || hipotenusa <= altura) {
            setResultado("La hipotenusa no puede ser menor o igual a la base o altura.");
            return;
        }

        if(hipotenusa != 0 && (base != 0 && altura == 0)) {
            altura = Math.sqrt(Math.pow(hipotenusa, 2) - Math.pow(base, 2));
        }

        if(hipotenusa != 0 && (altura != 0 && base == 0)) {
            base = Math.sqrt((Math.pow(hipotenusa, 2) - Math.pow(altura, 2)));
        }

        setPerimetro(base + altura + hipotenusa);

        setArea((base * altura) / 2);

        setResultado(("Perímetro = " + getPerimetro() + getSalto2() + "Área = " + getArea()));
    }
}
