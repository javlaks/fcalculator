/*
 * @(#)GeoGUI.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc.apps.geom;

import java.awt.event.KeyEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.fc.apps.geom.cuerpos.Cubo;
import org.fc.apps.geom.planos.Circulo;
import org.fc.apps.geom.planos.Cuadrado;
import org.fc.apps.geom.planos.Rectangulo;
import org.fc.apps.geom.planos.TrianguloRectangulo;
import org.fc.utils.data.Errores;
import org.fc.utils.gui.FCFrame;
import org.fc.utils.numbers.Decimales;

/**
 * This class contains component to create the Geo GUI.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 1.0
 * @version 4.0
 */
public class GeoGUI extends FCFrame {
    private static final long serialVersionUID = -4699163414981106771L;
    private String selectedFig = "";
    private Circulo circulo;
    private Cuadrado cuadrado;
    private Rectangulo rectangulo;
    private Cubo cubo;
    private TrianguloRectangulo triangulorectangulo;
    private JTextField campoValor;
    private double lado, base, altura, radio, hipotenusa, diagonal, diametro;

    public GeoGUI() {
        System.gc();
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelGeo = new javax.swing.JPanel();
        panelOp = new javax.swing.JPanel();
        opc = new javax.swing.JComboBox();
        cerrar = new javax.swing.JButton();
        calcular = new javax.swing.JButton();
        panelVal = new javax.swing.JPanel();
        primer = new javax.swing.JLabel();
        tercero = new javax.swing.JLabel();
        valor3 = new javax.swing.JTextField();
        valor1 = new javax.swing.JTextField();
        segundo = new javax.swing.JLabel();
        cuarto = new javax.swing.JLabel();
        valor2 = new javax.swing.JTextField();
        valor4 = new javax.swing.JTextField();
        desplazamiento = new javax.swing.JScrollPane();
        resultados = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        imagen = new javax.swing.JLabel();
        botonLimpiar = new javax.swing.JButton();

        setTitle("Geometría");
        setName("geome"); // NOI18N
        setResizable(false);

        panelOp.setBackground(new java.awt.Color(255, 255, 255));
        panelOp.setBorder(javax.swing.BorderFactory.createTitledBorder("Opciones"));
        panelOp.setOpaque(false);

        opc.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccionar...", "Círculo", "Cuadrado", "Cubo", "Rectángulo", "Triángulo Rectángulo" }));
        opc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelOpLayout = new javax.swing.GroupLayout(panelOp);
        panelOp.setLayout(panelOpLayout);
        panelOpLayout.setHorizontalGroup(
            panelOpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelOpLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(opc, 0, 319, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelOpLayout.setVerticalGroup(
            panelOpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOpLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(opc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
        );

        cerrar.setMnemonic('c');
        cerrar.setText("Cerrar");
        cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarActionPerformed(evt);
            }
        });

        calcular.setMnemonic('a');
        calcular.setText("Calcular");
        calcular.setEnabled(false);
        calcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calcularActionPerformed(evt);
            }
        });

        panelVal.setBackground(new java.awt.Color(255, 255, 255));
        panelVal.setBorder(javax.swing.BorderFactory.createTitledBorder("Valores"));
        panelVal.setOpaque(false);

        primer.setText("Valor 1");

        tercero.setText("Valor 3");

        valor3.setEnabled(false);
        valor3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                withFocus(evt);
            }
        });
        valor3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                withKeyPressed(evt);
            }
        });

        valor1.setDocument(new Decimales());
        valor1.setEnabled(false);
        valor1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                withFocus(evt);
            }
        });
        valor1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                withKeyPressed(evt);
            }
        });

        segundo.setText("Valor 2");

        cuarto.setText("Valor 4");

        valor2.setDocument(new Decimales());
        valor2.setEnabled(false);
        valor2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                withFocus(evt);
            }
        });
        valor2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                withKeyPressed(evt);
            }
        });

        valor4.setEnabled(false);
        valor4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                withFocus(evt);
            }
        });
        valor4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                withKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelValLayout = new javax.swing.GroupLayout(panelVal);
        panelVal.setLayout(panelValLayout);
        panelValLayout.setHorizontalGroup(
            panelValLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelValLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelValLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(valor1, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                    .addComponent(valor3, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                    .addComponent(primer, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                    .addComponent(tercero, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE))
                .addGap(26, 26, 26)
                .addGroup(panelValLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(valor4, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                    .addComponent(valor2, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                    .addComponent(segundo, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                    .addComponent(cuarto, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelValLayout.setVerticalGroup(
            panelValLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelValLayout.createSequentialGroup()
                .addGroup(panelValLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(primer)
                    .addComponent(segundo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelValLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(valor2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(valor1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelValLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tercero)
                    .addComponent(cuarto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelValLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valor3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(valor4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        desplazamiento.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultados"));
        desplazamiento.setOpaque(false);

        resultados.setColumns(20);
        resultados.setEditable(false);
        resultados.setLineWrap(true);
        resultados.setRows(5);
        resultados.setOpaque(false);
        desplazamiento.setViewportView(resultados);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Figura"));
        jPanel1.setOpaque(false);

        imagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/fc/files/images/geom/Geometria.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(imagen, javax.swing.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(imagen, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
        );

        botonLimpiar.setText("Limpiar");
        botonLimpiar.setToolTipText("Limpia los campos de datos y el área de resultados.");
        botonLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonLimpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelGeoLayout = new javax.swing.GroupLayout(panelGeo);
        panelGeo.setLayout(panelGeoLayout);
        panelGeoLayout.setHorizontalGroup(
            panelGeoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGeoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelGeoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelOp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelGeoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(desplazamiento, javax.swing.GroupLayout.DEFAULT_SIZE, 440, Short.MAX_VALUE)
                    .addComponent(panelVal, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelGeoLayout.createSequentialGroup()
                        .addComponent(calcular)
                        .addGap(18, 18, 18)
                        .addComponent(botonLimpiar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 223, Short.MAX_VALUE)
                        .addComponent(cerrar)))
                .addContainerGap())
        );
        panelGeoLayout.setVerticalGroup(
            panelGeoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGeoLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(panelGeoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelGeoLayout.createSequentialGroup()
                        .addComponent(panelOp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelGeoLayout.createSequentialGroup()
                        .addComponent(panelVal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(desplazamiento)
                        .addGap(18, 18, 18)
                        .addGroup(panelGeoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(calcular)
                            .addComponent(cerrar)
                            .addComponent(botonLimpiar))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGeo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGeo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void calcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calcularActionPerformed
        calcular();
}//GEN-LAST:event_calcularActionPerformed

    private void cerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarActionPerformed

        dispose();
    }//GEN-LAST:event_cerrarActionPerformed

    private void opcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcActionPerformed
        clean();

        selectedFig = String.valueOf(opc.getSelectedItem());
        switch(selectedFig) {
            case "Círculo":
                imagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/fc/files/images/geom/Circulo.png")));
                setStatus(valor1, true, primer, "radio");
                setStatus(valor2, true, segundo, "diametro");
                setStatus(valor3, false, tercero, "Valor 3");
                setStatus(valor4, false, cuarto, "Valor 4");
                break;
            case "Cuadrado":
                imagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/fc/files/images/geom/Cuadrado.png")));
                setStatus(valor1, true, primer, "lado");
                setStatus(valor2, true, segundo, "diagonal");
                setStatus(valor3, false, tercero, "Valor 3");
                setStatus(valor4, false, cuarto, "Valor 4");
                break;
            case "Cubo":
                imagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/fc/files/images/geom/Cubo.png")));
                setStatus(valor1, true, primer, "lado");
                setStatus(valor2, true, segundo, "diagonal");
                setStatus(valor3, false, tercero, "Valor 3");
                setStatus(valor4, false, cuarto, "Valor 4");
                break;
            case "Rectángulo":
                imagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/fc/files/images/geom/Rectangulo.png")));
                setStatus(valor1, true, primer, "base");
                setStatus(valor2, true, segundo, "altura");
                setStatus(valor3, true, tercero, "diagonal");
                setStatus(valor4, false, cuarto, "Valor 4");
                break;
            case "Triángulo Rectángulo":
                imagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/fc/files/images/geom/TrianguloRectangulo.png")));
                setStatus(valor1, true, primer, "lado a (base)");
                setStatus(valor2, true, segundo, "lado b (altura)");
                setStatus(valor3, true, tercero, "lado c (hipotenusa)");
                setStatus(valor4, false, cuarto, "Valor 4");
                break;
            case "Seleccionar...":
                imagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/fc/files/images/geom/Geometria.png")));
                setStatus(valor1, false, primer, "Valor 1");
                setStatus(valor2, false, segundo, "Valor 2");
                setStatus(valor3, false, tercero, "Valor 3");
                setStatus(valor4, false, cuarto, "Valor 4");
                calcular.setEnabled(false);
                return;
        }
        calcular.setEnabled(true);
    }//GEN-LAST:event_opcActionPerformed

    private void withFocus(java.awt.event.FocusEvent evt)//GEN-FIRST:event_withFocus
    {//GEN-HEADEREND:event_withFocus
        ((JTextField) evt.getComponent()).selectAll();

        campoValor = (JTextField) evt.getComponent();
    }//GEN-LAST:event_withFocus

    private void withKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_withKeyPressed
    {//GEN-HEADEREND:event_withKeyPressed
        int key = evt.getKeyCode();
        if(key == KeyEvent.VK_ENTER) {
            calcular();
        }
    }//GEN-LAST:event_withKeyPressed

    private void botonLimpiarActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_botonLimpiarActionPerformed
    {//GEN-HEADEREND:event_botonLimpiarActionPerformed
        clean();
    }//GEN-LAST:event_botonLimpiarActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonLimpiar;
    private javax.swing.JButton calcular;
    private javax.swing.JButton cerrar;
    private javax.swing.JLabel cuarto;
    private javax.swing.JScrollPane desplazamiento;
    private javax.swing.JLabel imagen;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JComboBox opc;
    private javax.swing.JPanel panelGeo;
    private javax.swing.JPanel panelOp;
    private javax.swing.JPanel panelVal;
    private javax.swing.JLabel primer;
    private javax.swing.JTextArea resultados;
    private javax.swing.JLabel segundo;
    private javax.swing.JLabel tercero;
    private javax.swing.JTextField valor1;
    private javax.swing.JTextField valor2;
    private javax.swing.JTextField valor3;
    private javax.swing.JTextField valor4;
    // End of variables declaration//GEN-END:variables

    private String getStrings(double value) {
        return String.valueOf(value);
    }

    private void clean() {
        valor1.setText("");
        valor2.setText("");
        valor3.setText("");
        valor4.setText("");

        resultados.setText("");
    }

    private void setStatus(JTextField campo, boolean status, JLabel etiqueta, String name) {
        campo.setEnabled(status);
        etiqueta.setText(name);
    }

    private void calcular() {
        try {
            switch(selectedFig) {
                case "Círculo":
                    getDatosCirculo();
                    circulo = new Circulo(radio, diametro);
                    setDatosCirculo();
                    break;
                case "Cuadrado":
                    getDatosCuadrado();
                    cuadrado = new Cuadrado(lado, diagonal);
                    setDatosCuadrado();
                    break;
                case "Cubo":
                    getDatosCubo();
                    cubo = new Cubo(lado, diagonal);
                    setDatosCubo();
                    break;
                case "Rectángulo":
                    getDatosRectangulo();
                    rectangulo = new Rectangulo(base, altura, diagonal);
                    setDatosRectangulo();
                    break;
                case "Triángulo Rectángulo":
                    getDatosTrianguloRectangulo();
                    triangulorectangulo = new TrianguloRectangulo(base, altura, hipotenusa);
                    setDatosTrianguloRectangulo();
                    break;
            }

            campoValor.requestFocus();
            campoValor.selectAll();
        } catch(NumberFormatException exp) {
            checkOptions();
            resultados.setText(Errores.errorDecimal());
        }
    }

    private void getDatosCirculo() {
        try {
            if(campoValor == valor1) {
                radio = Double.parseDouble(valor1.getText());
                diametro = 0;
            }
            if(campoValor == valor2) {
                diametro = Double.parseDouble(valor2.getText());
                radio = 0;
            }
        } catch(NumberFormatException exp) {
            checkOptions();
            resultados.setText(Errores.errorDecimal());
        }
    }

    private void setDatosCirculo() {
        valor1.setText(getStrings(circulo.getRadio()));
        valor2.setText(getStrings(circulo.getDiametro()));

        resultados.setText(circulo.getResultado());
    }

    private void getDatosCuadrado() {
        try {
            if(campoValor == valor1) {
                lado = Double.parseDouble(valor1.getText());
                diagonal = 0;
            }
            if(campoValor == valor2) {
                diagonal = Double.parseDouble(valor2.getText());
                lado = 0;
            }
        } catch(NumberFormatException exp) {
            checkOptions();
            resultados.setText(Errores.errorDecimal());
        }
    }

    private void setDatosCuadrado() {
        valor1.setText(getStrings(cuadrado.getLado()));
        valor2.setText(getStrings(cuadrado.getDiagonal()));

        resultados.setText(cuadrado.getResultado());
    }

    private void getDatosRectangulo() {
        try {
            if(valor1.getText().isEmpty() || valor1.getText().equals("0")) {
                base = 0;
            } else {
                base = Double.parseDouble(valor1.getText());
            }

            if(valor2.getText().isEmpty() || valor2.getText().equals("0")) {
                altura = 0;
            } else {
                altura = Double.parseDouble(valor2.getText());
            }

            if(valor3.getText().isEmpty() || valor3.getText().equals("0")) {
                diagonal = 0;
            } else {
                diagonal = Double.parseDouble(valor3.getText());
            }
        } catch(NumberFormatException exp) {
            checkOptions();
            resultados.setText(Errores.errorDecimal());
        }
    }

    private void setDatosRectangulo() {
        valor1.setText(getStrings(rectangulo.getBase()));
        valor2.setText(getStrings(rectangulo.getAltura()));
        valor3.setText(getStrings(rectangulo.getDiagonal()));

        resultados.setText(rectangulo.getResultado());
    }

    private void getDatosCubo() {
        try {
            if(campoValor == valor1) {
                lado = Double.parseDouble(valor1.getText());
                diagonal = 0;
            }
            if(campoValor == valor2) {
                diagonal = Double.parseDouble(valor2.getText());
                lado = 0;
            }
        } catch(NumberFormatException exp) {
            checkOptions();
            resultados.setText(Errores.errorDecimal());
        }
    }

    private void setDatosCubo() {
        valor1.setText(getStrings(cubo.getLado()));
        valor2.setText(getStrings(cubo.getDiagonal()));

        resultados.setText(cubo.getResultado());
    }

    private void getDatosTrianguloRectangulo() {
        try {
            if(valor1.getText().isEmpty() || valor1.getText().equals("0")) {
                base = 0;
            } else {
                base = Double.parseDouble(valor1.getText());
            }

            if(valor2.getText().isEmpty() || valor2.getText().equals("0")) {
                altura = 0;
            } else {
                altura = Double.parseDouble(valor2.getText());
            }

            if(valor3.getText().isEmpty() || valor3.getText().equals("0")) {
                hipotenusa = 0;
            } else {
                hipotenusa = Double.parseDouble(valor3.getText());
            }
        } catch(NumberFormatException exp) {
            checkOptions();
            resultados.setText(Errores.errorDecimal());
        }
    }

    private void setDatosTrianguloRectangulo() {
        valor1.setText(getStrings(triangulorectangulo.getBase()));
        valor2.setText(getStrings(triangulorectangulo.getAltura()));
        valor3.setText(getStrings(triangulorectangulo.getHipotenusa()));

        resultados.setText(triangulorectangulo.getResultado());
    }
}
