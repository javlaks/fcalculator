/*
 * @(#)Main.java
 *
 * FCalculator
 * Copyright (C) 2008-2013 Victor Pineda
 *
 * This file is part of FCalculator.
 *
 * FCalculator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCalculator; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * For more information, please see http://fcalculator.sourceforge.net/licencia.html
 */
package org.fc;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import org.fc.ex.ErrorData;
import org.fc.utils.Utilities;
import org.fc.utils.configuration.CheckConfiguration;
import org.fc.utils.configuration.Configuration;
import org.fc.utils.look.LooksAndFeels;
import org.jdesktop.swingx.JXErrorPane;

/**
 * This class contains our main method.
 *
 * @author Victor Pineda
 * @author http://vscorpionblack.blogspot.com
 * @since 4.0
 * @version 4.1
 */
public class Main {
    /**
     * Reference to the class manager in charge of our look and feel.
     */
    private static LooksAndFeels look;

    /**
     * This is our main method.
     *
     * @param args Receives the method arguments.
     */
    public static void main(String[] args) {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    configurationExits();
                    look = new LooksAndFeels();

                    JFrame.setDefaultLookAndFeelDecorated(true);
                    JDialog.setDefaultLookAndFeelDecorated(true);

                    setLook();

                    FCSplash splash = new FCSplash(
                            Utilities.getImage("/org/fc/files/images/splash/Splash.png"));
                    splash.execute();
                }
            });
        } catch(InterruptedException | InvocationTargetException ex) {
            JXErrorPane.showDialog(null, new ErrorData(ex).getErrorInfo());
        }
    }

    /**
     * This method allows us to get the latest configuration of our properties file to set the look
     * and feel to our program.
     */
    private static void setLook() {
        look.setLook(Configuration.getPropiedad("lookandfeel"));
    }

    /**
     * Here we check if we have our configuration file in the same directory of our JAR. If not
     * found, one is created with the default settings.
     */
    private static void configurationExits() {
        File archivo = new File(Utilities.getOptionsFilePath());

        if(archivo.exists()) {
            new CheckConfiguration().check();
        } else {
            Configuration.setDefaults();
        }
    }

    /**
     * Default constructor
     */
    private Main() {
    }
}
